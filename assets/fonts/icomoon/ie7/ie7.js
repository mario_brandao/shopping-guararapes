/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'i-folder-stroke': '&#xe927;',
		'i-store': '&#xe926;',
		'i-envelope': '&#xe925;',
		'i-phone': '&#xe924;',
		'i-query': '&#xe91d;',
		'i-car-wheel': '&#xe91e;',
		'i-accessibility': '&#xe91f;',
		'i-wifi': '&#xe920;',
		'i-mic': '&#xe921;',
		'i-bill': '&#xe922;',
		'i-family': '&#xe923;',
		'i-close': '&#xe91c;',
		'i-trophy': '&#xe918;',
		'i-clapperboard': '&#xe919;',
		'i-masks': '&#xe91a;',
		'i-screen': '&#xe91b;',
		'i-play': '&#xe917;',
		'i-info': '&#xe916;',
		'i-settings': '&#xe915;',
		'i-apple': '&#xe900;',
		'i-arrow': '&#xe901;',
		'i-clock': '&#xe902;',
		'i-contact': '&#xe903;',
		'i-drop-arrow': '&#xe904;',
		'i-events': '&#xe905;',
		'i-facebook': '&#xe906;',
		'i-folder': '&#xe907;',
		'i-gastronomy': '&#xe908;',
		'i-google-play': '&#xe909;',
		'i-google-plus': '&#xe90a;',
		'i-happy': '&#xe90b;',
		'i-instagram': '&#xe90c;',
		'i-leaf': '&#xe90d;',
		'i-lock': '&#xe90e;',
		'i-marker': '&#xe90f;',
		'i-movie-roll': '&#xe910;',
		'i-points': '&#xe911;',
		'i-search': '&#xe912;',
		'i-twitter': '&#xe913;',
		'i-youtube': '&#xe914;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/i-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
