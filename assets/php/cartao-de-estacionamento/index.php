<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Defining the meta
$meta	= ( defined( 'METADATA_SUSTENTABILIDADE' ) ) ? METADATA_SUSTENTABILIDADE : null;

//Showing the page
$template->show();