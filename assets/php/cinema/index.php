<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_CINEMA' ) ) ? METADATA_CINEMA : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Institutional
try{

	//Defining the object to template
	$template->OBJ_INSTITUTIONAL	= SearchController::byId( 'institutional-cinema' );

}catch( Exception $e ){}

//Cinema
try{

	//Defining the records
	$records	= json_decode( file_get_contents( BASE_URL_CINEMA . '/list.json' ) );

	//Listing
	foreach( $records->movies as $movie ){

		//Defining the object to template
		$template->OBJ_MOVIE	= $movie;

		//Defining
		$has3d					= false;

		//Listing the rooms
		foreach( $movie->rooms as $room ){

			//Listing the sessions
			foreach( $room->sessions as $session ){

				//Listing
				foreach( $session->types as $type )
					//Checking
					if( $type == '3D' )
						//Defining
						$has3d	= true;

			}

		}

		//Checking
		if( $has3d )
			//Showing the block
			$template->block( '3D' );

		//Showing the block
		$template->block( 'MOVIE' );

	}

}catch( Exception $e ){};

//Defining some variables
$template->BASE_URL_IMG_SHARE	= BASE_URL_IMG_SHARE;

//Showing the page
$template->show();