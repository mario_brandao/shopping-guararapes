<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Institutional
try{

	//Defining the object to template
	$template->OBJ_INSTITUTIONAL	= SearchController::byId( 'institutional-cinema' );

}catch( Exception $e ){}

//Cinema
try{

	//Defining
	$json	= json_decode( file_get_contents( BASE_URL_CINEMA . '/movie/' . $page->slug . '.json' ) );

	//Checking
	if( $json ){

		//Defining the object to template
		$template->OBJ_MOVIE	= $json;

		//Defining the metadata
		$template->DESCRIPTION	= Util::defineDescription( $json->synopsis );

		//Checking
		if( !is_null( $json->trailer ) )
			//Showing the block
			$template->block( 'TRAILER' );

		//Defining
		$counter				= 0;

		//Listing
		foreach( $json->rooms as $date => $rooms ){

			//Defining the date to template
			$template->DATE				= Util::formatDate( $date, 'cinema' );

			//Defining the day to template
			$template->DAY				= Util::formatDate( $date, 'day-of-week' );

			//Defining the counter to template
			$template->COUNTER			= $counter;

			//Defining the class to template
			$template->COUNTER_CLASS	= ( $counter == 0 ) ? 'active' : null;

			//Listing
			foreach( $rooms as $room ){

				//Defining the object to template
				$template->OBJ_ROOM	= $room;

				//Defining
				$times				= [];
				$types				= [];

				//Listing
				foreach( $room->sessions as $session ){

					//Defining the times
					$times[]	= $session->time;

					//Listing
					foreach( $session->types as $type )
						//Defining the types
						$types[]	= $type;

				}

				//Defining
				$types				= Util::defineMovieType( $types );

				//Checking
				if( isset( $types->dub ) )
					//Showing the block to template
					$template->block( 'DUB' );

				//Checking
				if( isset( $types->leg ) )
					//Showing the block to template
					$template->block( 'LEG' );

				//Checking
				if( isset( $types->has3d ) )
					//Showing the block to template
					$template->block( 'HAS_3D' );

				//Checking
				if( isset( $types->nac ) )
					//Showing the block to template
					$template->block( 'NAC' );

				//Defining the time to template
				$template->TIMES	= implode( ', ', $times );

				//Showing the block
				$template->block( 'ROOM' );

			}

			//Showing the blocks
			$template->block( 'DATES' );
			$template->block( 'SESSIONS' );

			//Defining
			$counter++;

		}


	}

}catch( Exception $e ){}

//Cinema
try{

	//Defining the records
	$records	= json_decode( file_get_contents( BASE_URL_CINEMA . '/list.json' ) );

	//Listing
	foreach( $records->movies as $movie ){

		//Defining the object to template
		$template->OBJ_OTHER_MOVIE	= $movie;

		//Defining
		$has3d					= false;

		//Listing the rooms
		foreach( $movie->rooms as $room ){

			//Listing the sessions
			foreach( $room->sessions as $session ){

				//Listing
				foreach( $session->types as $type )
					//Checking
					if( $type == '3D' )
						//Defining
						$has3d	= true;

			}

		}

		//Checking
		if( $has3d )
			//Showing the block
			$template->block( '3D' );

		//Showing the block
		$template->block( 'MOVIE' );

	}

}catch( Exception $e ){};

//Showing the page
$template->show();