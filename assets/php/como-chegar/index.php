<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Showing the page
$template->show();