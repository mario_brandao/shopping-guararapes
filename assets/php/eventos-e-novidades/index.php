<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_NOTICIA' ) ) ? METADATA_NOTICIA : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//News
try{

	//Defining the limit
	$limit						= ( $page->page > 0 ) ? ( $page->page - 1 ) * RECORDS_PER_PAGE : 0;

	//Defining the quantity
	$quantity					= CountController::count( 'news' );

	//Defining the pagination
	$pagination					= new Pagination( $limit, RECORDS_PER_PAGE, $quantity, $page->module );

	//Defining
	$records					= ListController::orderlyLimited( 'news', 'date-desc', $limit );

	//Defining the pagination to template
	$template->OBJ_PAGINATION	= $pagination;

	//Listing
	foreach( $records as $record ){

		//Defining the object to template
		$template->OBJ_NEWS	= $record;

		//Defining the date to template
		$template->DATE		= Util::formatDate( $record->date, 'site' );

		//Defining the image to template
		$template->IMAGE	= ( !is_null( $record->image ) ) ? BASE_URL_REPOSITORY . '/news/medium-' . $record->image : BASE_URL_IMG_IMAGE_NO_NEWS_MEDIUM;

		//Checking
		if( !is_null( $record->subtitle ) )
			//Showing the block
			$template->block( 'HAS_SUBTITLE' );

		//Showing the block to template
		$template->block( 'NEWS' );

	}

}catch( Exception $e ){}

//Defining some variables
$template->BASE_URL_IMG_SHARE	= BASE_URL_IMG_SHARE;

//Showing the page
$template->show();