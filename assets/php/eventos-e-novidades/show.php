<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Sustainability
try{

	//Defining
	$object					= SearchController::bySlug( 'news', $page->slug );

	//Defining the object to template
	$template->OBJ_NEWS		= $object;

	//Defining the description to template
	$template->DESCRIPTION	= Util::defineDescription( $object->text );

	//Defining the image to template
	$template->IMAGE		= ( !is_null( $object->image ) ) ? BASE_URL_REPOSITORY . '/news/' . $object->image : BASE_URL_IMG_IMAGE_NO_NEWS_BIG;

	//Defining the date to template
	$template->DATE			= Util::formatDate( $object->date, 'site' );

	//Defining
	$records				= ListController::notActiveByIdOrderlyByQuantity( 'news', $object->id, 'date-desc' );

	//Checking
	if( !is_null( $records ) )
		//Listing
		foreach( $records as $record ){

			//Defining the object to template
			$template->OBJ_OTHER_NEWS	= $record;

			//Defining the date to template
			$template->OTHER_NEWS_DATE	= Util::formatDate( $record->date, 'site' );

			//Defining the image to template
			$template->OTHER_NEWS_IMAGE	= ( !is_null( $record->image ) ) ? BASE_URL_REPOSITORY . '/news/small-' . $record->image : BASE_URL_IMG_IMAGE_NO_NEWS_SMALL;

			//Showing the block
			$template->block( 'OTHER_NEWS' );

		}

}catch( Exception $e ){}

//Showing the page
$template->show();