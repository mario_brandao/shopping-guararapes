<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_HOME' ) ) ? METADATA_HOME : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Banners
try{

	//Defining
	$records	= ListController::orderly( 'banner', 'sequence' );

	//Listing
	foreach( $records as $record ){

		//Defining the object to template
		$template->OBJ_BANNER	= $record;

		//Showing the block
		$template->block( 'BANNER' );

	}

}catch( Exception $e ){}

//Cinema
try{

	//Defining the records
	$records	= json_decode( file_get_contents( BASE_URL_CINEMA . '/list.json' ) );

	//Listing
	foreach( $records->movies as $movie ){

		//Defining the object to template
		$template->OBJ_MOVIE	= $movie;

		//Defining
		$has3d					= false;

		//Listing the rooms
		foreach( $movie->rooms as $room ){

			//Listing the sessions
			foreach( $room->sessions as $session ){

				//Listing
				foreach( $session->types as $type )
					//Checking
					if( $type == '3D' )
						//Defining
						$has3d	= true;

			}

		}

		//Checking
		if( $has3d )
			//Showing the block
			$template->block( '3D' );

		//Showing the block
		$template->block( 'MOVIE' );

	}

}catch( Exception $e ){};

//Sustainability
try{

	//Defining
	$records	= ListController::orderly( 'sustainability' );

	//Listing
	foreach( $records as $record ){

		//Defining the object to template
		$template->OBJ_SUSTAINABILITY	= $record;

		//Defining the text to template
		$template->SUSTAINABILITY_TEXT	= Util::defineDescription( $record->text );

		//Showing the block
		$template->block( 'SUSTAINABILITY' );

	}

}catch( Exception $e ){}

//News
try{

	//Defining
	$records	= ListController::orderly( 'news', 'date-desc' );

	//Listing
	foreach( $records as $record ){

		//Defining the object to template
		$template->OBJ_NEWS		= $record;

		//Defining the date to template
		$template->NEWS_DATE	= Util::formatDate( $record->date, 'site' );

		//Defining the image to template
		$template->NEWS_IMAGE	= ( !is_null( $record->image ) ) ? BASE_URL_REPOSITORY . '/news/small-' . $record->image : BASE_URL_IMG_IMAGE_NO_NEWS_SMALL;

		//Showing the block
		$template->block( 'NEWS' );

	}

}catch( Exception $e ){}

//Defining some variables
$template->BASE_URL_IMG_SHARE	= BASE_URL_IMG_SHARE;

//Showing the page
$template->show();