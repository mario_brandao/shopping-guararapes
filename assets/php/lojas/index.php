<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_LOJA' ) ) ? METADATA_LOJA : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Stores
try{

	//Defining the limit
	$limit						= ( $page->page > 0 ) ? ( $page->page - 1 ) * RECORDS_PER_PAGE : 0;

	//Defining the quantity
	$quantity					= CountController::count();

	//Defining the pagination
	$pagination					= new Pagination( $limit, RECORDS_PER_PAGE, $quantity, $page->module );

	//Defining
	$records					= ListController::orderlyLimited( 'store', 'name', $limit );

	//Defining the pagination to template
	$template->OBJ_PAGINATION	= $pagination;

	//Listing
	foreach( $records as $record ){

		//Defining the object to template
		$template->OBJ_STORE	= $record;

		//Checking
		if( !is_null( $record->phone ) ){

			//Defining the fomratted's phone to template
			$template->STORE_PHONE_FORMATTED	= Util::formatPhone( $record->phone );

			//Showing the block
			$template->block( 'HAS_PHONE' );
		}

		//Showing the block
		$template->block( 'STORE' );

	}

}catch( Exception $e ){}

//Defining some variabels
$template->BASE_URL_IMG_SHARE	= BASE_URL_IMG_SHARE;

//Showing the page
$template->show();