<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Store
try{

	//Defining
	$object					= SearchController::bySlug( 'store', $page->slug );

	//Defining the object to template
	$template->OBJ_STORE	= $object;

	//Defining the description to template
	$template->DESCRIPTION	= Util::defineDescription( $object->description );

	//Checking
	if( !is_null( $object->phone ) ){

		//Defining the formatted phone to template
		$template->STORE_PHONE_FORMATTED	= Util::formatPhone( $object->phone );

		//Showing the block
		$template->block( 'HAS_PHONE' );

	}

	//Checking
	if( !is_null( $object->facebook ) )
		//Showing the block
		$template->block( 'HAS_FACEBOOK' );

	//Checking
	if( !is_null( $object->email ) )
		//Showing the block
		$template->block( 'HAS_EMAIL' );

	//Defining
	$records	= ListController::notActiveByIdByTypeOrderlyByQuantity( 'store-by-category-by-type', $object->id, $object->type, 'random' );

	//Checking
	if( !is_null( $records ) )
		//Listing
		foreach( $records as $record ){

			//Defining the object to template
			$template->OBJ_OTHER_STORES	= $record;

			//Checking
			if( !is_null( $record->phone ) ){

				//Defining the formatted phone to template
				$template->OTHER_STORES_PHONE_FORMATTED	= Util::formatPhone( $record->phone );

				//Showing the block
				$template->block( 'HAS_OTHER_STORES_PHONE' );

			}

			//Showing the block
			$template->block( 'OTHER_STORES' );

		}

}catch( Exception $e ){

	//Redirecionando
	header( 'Location: ' . BASE_URL . '/lojas' );

}

//Showing the page
$template->show();