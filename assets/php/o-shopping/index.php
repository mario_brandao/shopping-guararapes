<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_INSTITUCIONAL' ) ) ? METADATA_INSTITUCIONAL : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Institutional
try{

	//Defining
	$object							= SearchController::byId( 'institutional' );

	//Defining the object to template
	$template->OBJ_INSTITUTIONAL	= $object;

	//Checking
	if( !is_null( $object->images ) )
		//Listing
		foreach( $object->images as $image ){

			//Defining the object to template
			$template->OBJ_IMAGE	= $image;

			//Showing the block
			$template->block( 'IMAGES' );

		}

}catch( Exception $e ){}

//Showing the page
$template->show();