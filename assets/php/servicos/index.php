<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_SERVICO' ) ) ? METADATA_SERVICO : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Services
try{

	//Defining
	$records	= ListController::orderly( 'service', 'title' );

	//Listing
	foreach( $records as $counter => $record ){

		//Defining the object to template
		$template->OBJ_SERVICE	= $record;

		//Defining the counter to template
		$template->COUNTER		= $counter;

		//Checking
		if( !is_null( $record->subtitle ) )
			//Showing the block
			$template->block( 'SUBTITLE' );

		//Showing the blocks
		$template->block( 'SERVICE' );
		$template->block( 'DESCRIPTION' );

	}

}catch( Exception $e ){}

//Showing the page
$template->show();