<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_SUSTENTABILIDADE' ) ) ? METADATA_SUSTENTABILIDADE : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Institutional
try{

	//Defining the object to template
	$template->OBJ_INSTITUTIONAL	= SearchController::byId( 'institutional-sustainability' );

}catch( Exception $e ){}

//Sustainability
try{

	//Defining
	$records	= ListController::orderly( 'sustainability', 'title' );

	//Listing
	foreach( $records as $record ){

		//Defining the object to template
		$template->OBJ_SUSTAINABILITY	= $record;

		//Defining the text to template
		$template->TEXT					= Util::defineDescription( $record->text, 25 );

		//Showing the block to template
		$template->block( 'SUSTAINABILITY' );

	}

}catch( Exception $e ){}

//Defining some variabels
$template->BASE_URL_IMG_SHARE	= BASE_URL_IMG_SHARE;

//Showing the page
$template->show();