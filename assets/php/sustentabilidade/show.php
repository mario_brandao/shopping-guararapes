<?php
/**
 * Showing the page
 */
//Defining the template
$template	= new Template( $page );

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Sustainability
try{

	//Defining
	$object	= SearchController::bySlug( 'sustainability', $page->slug );

	//Defining the object to template
	$template->OBJ_SUSTAINABILITY	= $object;

	//Defining the description to template
	$template->DESCRIPTION			= Util::defineDescription( $object->text );

}catch( Exception $e ){}

//Showing the page
$template->show();