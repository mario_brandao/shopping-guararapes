<?php
/**
 * Showing the page
 */

//Defining the template
$template	= new Template( $page );

//Defining the meta
$meta		= ( defined( 'METADATA_CONTATO' ) ) ? METADATA_CONTATO : null;

//Including the template's variables
require( BASE_DIR . '/include/variables.php' );

//Institutional
try{

	//Defining the object to template
	$template->OBJ_INSTITUTIONAL	= SearchController::byId( 'institutional-contact' );

}catch( Exception $e ){}

//Institutional
try{

	//Defining the records
	$records	= ListController::orderly( 'subject', 'title' );

	//Listing
	foreach( $records as $record ){

		//Defining the object to template
		$template->OBJ_SUBJECT	= $record;

		//Showing the block
		$template->block( 'SUBJECT' );

	}

}catch( Exception $e ){}

$template->CAPTCHA_KEY_SITE		= CAPTCHA_KEY_SITE;

//Showing the page
$template->show();