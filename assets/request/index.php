<?php
/*
 * Request's file
 */

//Starting the session
session_start();

//Defining the timezone
date_default_timezone_set( 'America/Recife' );

//Defining the locale
setlocale( LC_TIME, 'ptBR', 'pt_BR', 'pt_BR.utf-8', 'portuguese' );

//Including the main's file
include( '../../include/constant.php' );
include( '../../vendor/autoload.php' );

//Checking the debug
if( !DEBUG )
	//Hiding the errors
	error_reporting( 0 );

//Setting
new Page( $_GET );