<?php
/*
 * Importing the cinema's sessions
 */

//Hiding the errors
error_reporting( 0 );

//Defining the location
date_default_timezone_set( 'America/Recife' );
setlocale( LC_TIME, 'ptBR', 'pt_BR', 'pt_BR.utf-8', 'portuguese' );

//Defining the time's limit
set_time_limit( 0 );

//Including the files
require_once( dirname( __FILE__ ) . '/../../../include/constant.php' );
require_once( dirname( __FILE__ ) . '/../../../vendor/autoload.php' );

try{

	//Importing
	ListController::importCinema();

}catch( Exception $e ){}