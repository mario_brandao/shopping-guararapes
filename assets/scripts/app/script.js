/*
* =============================
*   Interface functions
* =============================
*/
$(document).ready(function () {
  // ========================================
  // Variables
  // ========================================
  var mobile = $(window).width() > 767 ? false : true;
  var reCaptcha = $('.g-recaptcha');
  var reCaptchaLabel = $('.js-recaptcha-label');
  var recaptchaMessage = 'Por favor, confirme o reCAPTCHA';
  var termsMessage = 'O campo de termos é obrigatório!';
  var zoomState = 1;
  var zoomMax = 3;
  var zoomMin = 1;
  var storesJson = baseUrlRepository + '/json/store-site.json';
  var storeName = 'name';
  var urlStores = baseUrl + '/lojas/';
  var options = {
    url: storesJson, // Stores URL
    getValue: storeName,
    list: {
      match: {
        enabled: true
      },
      onChooseEvent: function () {
        // redirect
        var slug = $('.input-search').getSelectedItemData().slug;
        window.location.href = urlStores + slug;
      }
    },
    theme: 'square'
  };
  var optionsInternal = {
    url: storesJson, // Stores URL
    getValue: storeName,
    list: {
      match: {
        enabled: true
      },
      onSelectItemEvent: function () {
        // redirect
        var slug = $('.input-search-internal').getSelectedItemData().slug;
        window.location.href = urlStores + slug;
        console.log('asdaiduaudsd');
        console.log(slug);
      }
    },
    theme: 'square'
  };
  var zoomPlus = $('.js-zoom-plus');
  var zoomLess = $('.js-zoom-less');

  if ($('#parallax-container').length === 1) {
    var scene = document.getElementById('parallax-container');
    var parallax = new Parallax(scene);
  };
  

  // ========================================
  // Basic settings
  // ========================================
  $(window).on('resize', function () {
    mobile = $(window).width() > 767 ? false : true;
  });
  // ON LOAD
  $('.loader').fadeOut();
  inputsFx();
  
  // Togle mobile menu
  $('.js-menu-toggle').on('click', function (e) {
    e.preventDefault();
    $('body').toggleClass('blocked');
    $(this).toggleClass('open');
    $('.js-nav-collapse').toggleClass('open');
  });
  
  // Inputs fx
  function inputsFx() {
    var lWrap = $('.field-wrapper');
    
    if (lWrap.length > 0){
      // inputs in forms
      lWrap.each(function() {
        var t  = $(this),
        tInput = t.find('.field-input'),
        tLabel = t.find('.field-label');
        
        tInput.on('focus', function() {
          t.addClass('focused');
        });
        tInput.on('blur', function () {
          if (tInput.val() === '') {
            t.removeClass('focused');
          };
        });
        
      });
    };
  };
  
  // Setores header
  $('.chosen-header').chosen({
    disable_search_threshold: 10
  }).change(function(data) {
    // redirect
    window.location.href = baseUrl + '/lojas/' + data.target.value;
  });
  $('.chosen-store').chosen({
    disable_search_threshold: 10
  }).change(function (data) {
    // redirect
    window.location.href = baseUrl + '/lojas/' + data.target.value;
  });

  $('.input-search').easyAutocomplete(options);
  $('.input-search-internal').easyAutocomplete(optionsInternal);


  // ========================================
  // Sliders
  // ========================================
  // Slider Single
  $('.slider-single').slick({
    arrows: false,
    dots: true
  });

  // Slider Single
  $('.slider-single-arrow').slick({
    arrows: true,
    dots: false
  });

  // Slider cinema
  $('.slider-cinema').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
    ]
  });
  
  // Slider events
  $('.slider-events').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      }
    ]
  });
  

  // ========================================
  // DOM events
  // ========================================
  $('.js-tab').on('click', function(e) {
    e.preventDefault();
    var _this = $(this);
    var tabWrap = _this.parents('.js-tab-wrapper');
    var thisTarget = _this.attr('data-target');
    
    // Close other tabs
    tabWrap.find('.js-tab').removeClass('active');
    tabWrap.find('.js-tab-content').stop().slideUp().removeClass('active');

    // Open current atb
    _this.addClass('active');
    tabWrap.find(thisTarget).stop().slideDown().addClass('active');

  });

  $('.js-play').on('click', function(e) {
    e.preventDefault();
    var videoWrap = $(this).parents('.js-video-wrapper');
    var frameSrc  = videoWrap.attr('data-frame');

    videoWrap
      .append('<iframe src="' + frameSrc + '" allow="autoplay; encrypted-media" allowfullscreen></iframe>')
      .removeClass('stop');
  });

  $('.js-show-all').on('click', function (e) {
    e.preventDefault();
    var _this     = $(this);
    var showAfter = _this.parents('.js-text-wrapper').find('.sequence');

    showAfter.slideDown();
    _this.fadeOut();
  });

  $('.js-open-tooltip').on('click', function (e) {
    e.preventDefault();
    var _this = $(this);
    var tooltipOffset = _this.offset().top - $('.js-wrap-tooltips').offset().top + _this.outerHeight();
    var currTooltip = _this.next('.js-tooltip');

    $('.js-tooltip-origin').not(_this).css('margin-bottom', '2rem').removeClass('open'); // takes 300ms
    _this.addClass('open');
    $('.js-tooltip').not(currTooltip).stop().slideUp();  // takes 300ms

    if (mobile) {
      currTooltip.slideDown();
    } else {
      console.log('not mobile case');
      setTimeout(function () {
        var newTooltipOffset = _this.offset().top - $('.js-wrap-tooltips').offset().top + _this.outerHeight();
        currTooltip.slideDown(function () {
          _this.css('margin-bottom', 'calc(' + currTooltip.outerHeight() + 'px + 4rem)');
          currTooltip.css('top', newTooltipOffset);
        });
      }, 310);
    }

  });

  $('.js-close-tooltip').on('click', function (e) {
    e.preventDefault();
    var thisTarget =  $(this).parent('.js-tooltip');
    $('.js-tooltip-origin').css('margin-bottom', '2rem').removeClass('open');
    thisTarget.stop().slideUp();
  });

  $('.js-select-nav').on('change', function () {
    window.location.href = $(this).val();
  });

  $('.js-choose-file input').on('change', function () {
    var fileName = $(this).val();
    fileName = fileName.split('\\');
    fileName = fileName[fileName.length - 1];

    $(this).next('.file-name').html(fileName);
  });

  $('.js-show-more').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('open').siblings('.js-show-more-target').slideToggle();
  });

  $('.js-open-modal').on('click', function (e) {
    var target = $(this).attr('data-target');
    $(target).fadeIn();
  });
  $('.js-close-modal').on('click', function (e) {
    $(this).parents('.modal').fadeOut();
  });

  $('.js-input-drop').on('focus', function() {
    $('.js-drop-content').addClass('open');
  });
  $('.js-input-drop').on('blur', function () {
    setTimeout(function() {
      $('.js-drop-content').removeClass('open');
    }, 300);
  });
  

  if (mobile) {
    $('.js-dropdown').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('open').next('.js-dropdown-content').stop().slideToggle();
    });
  };

  // ========================================
  // Zoom functions
  // ========================================
  function disableControls(zoomState) {
    if (zoomState === zoomMax) {
      zoomPlus.addClass('disabled');
    } else {
      zoomPlus.removeClass('disabled');
    };

    if (zoomState === zoomMin) {
      zoomLess.addClass('disabled');
    } else {
      zoomLess.removeClass('disabled');
    };
  };

  zoomPlus.on('click', function(e) {
    e.preventDefault();
    if (zoomState < zoomMax) {
      zoomState += 1;
      $('.img-zoom').css('transform', 'scale(' + zoomState + ')');
    };
    disableControls(zoomState);
  });

  zoomLess.on('click', function(e) {
    e.preventDefault();
    if (zoomState > zoomMin) {
      zoomState -= 1;
      $('.img-zoom').css('transform', 'scale(' + zoomState + ')');
    };
    disableControls(zoomState);
  });

  // ========================================
  // Form validation
  // ========================================

  jQuery.validator.addMethod("cpf", function (value, element) {
    value = jQuery.trim(value);

    value = value.replace('.', '');
    value = value.replace('.', '');
    cpf = value.replace('-', '');
    while (cpf.length < 11) cpf = "0" + cpf;
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    var a = [];
    var b = new Number;
    var c = 11;
    for (i = 0; i < 11; i++) {
      a[i] = cpf.charAt(i);
      if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
    b = 0;
    c = 11;
    for (y = 0; y < 10; y++) b += (a[y] * c--);
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }

    var retorno = true;
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;

    return this.optional(element) || retorno;

  }, 'CPF inválido');

  function clearForm(form) {

    $(form)[0].reset();

    $(form).find('.focused').each(function() {
      $(this).removeClass('focused');
    });

    return false;
  };

  $('.js-form-news').validate({
    rules: {
      "email": {
        required: true,
        email: true
      }
    },
    onclick: function (el) {
      $(el).parents('.invalid').removeClass('invalid');
    },
    onkeyup: function (el) {
      $(el).parents('.invalid').removeClass('invalid');
    },
    highlight: function (el) {
      $(el).parents('.field-wrapper').addClass('invalid');
    },
    submitHandler: function (form) {
      $.ajax({
        method: 'POST',
        url: $(form).attr('action'),
        data: $(form).serialize(),
        dataType: 'json',
        beforeSend: function () {
          $('.js-btn-submit').attr('disabled', 'disabled');
        },
        success: function (resp) {
          alert(resp.responseText);
          console.log(resp);

          clearForm(form);
        },
        error: function (context) {
          alert(context.statusText + '. Status ' + context.status);
          console.log(context);

          if (context.status === 200) { // Validation status fix
            clearForm(form);
          };
        },
        complete: function () {
          $('.js-btn-submit').removeAttr('disabled');
        }
      });
      return false;
    }
  });

  $('.js-form-contact').validate({
    rules: {
      "nome": {
        required: true,
      },
      "email": {
        required: true,
        email: true
      },
      "telefone": {
        required: true,
      },
      "assunto": {
        required: true,
      },
      "mensagem": {
        required: true,
      },
      "interesse": {
        required: true,
      },
      "data_nascimento": {
        required: true,
      },
      "rg": {
        required: true
      },
      "cpf": {
        required: true,
        cpf: true
      },
      "nome_arquivo": {
        required: true,
      },
      "data": {
        required: true,
      },
      "numero_do_cartao": {
        required: true,
      },
      "placa": {
        required: true,
      },
      "celular": {
        required: true,
      },
      "usuario_interno": {
        required: true,
      },
      "termos": {
        required: true,
      },
    },
    onclick: function (el) {
      $(el).parents('.invalid').removeClass('invalid');
    },
    onkeyup: function (el) {
      $(el).parents('.invalid').removeClass('invalid');
    },
    highlight: function (el) {
      $(el).parents('.field-wrapper').addClass('invalid');

      // terms and conditions
      if ($(el).attr('type') === 'checkbox') {
        alert(termsMessage);
      };

      // reCaptcha validation
      if ($(document).find('.g-recaptcha-response').val() === '') {
        reCaptchaLabel.addClass('invalid');
      };
    },
    submitHandler: function (form) {
      // reCaptcha validation
      if ($(document).find('.g-recaptcha-response').val() === '') {
        reCaptchaLabel.addClass('invalid');
        alert(recaptchaMessage);
      } else {
        $.ajax({
          method: 'POST',
          url: $(form).attr('action'),
          data: $(form).serialize(),
          dataType: 'json',
          beforeSend: function () {
            $('.js-btn-submit').attr('disabled', 'disabled');
          },
          success: function (resp) {
            alert(resp.responseText);
            console.log(resp);

            clearForm(form);
          },
          error: function (context) {
            alert(context.statusText + '. Status ' + context.status);
            console.log(context);

            if (context.status === 200) { // Validation status fix
              clearForm(form);
            };
          },
          complete: function () {
            $('.js-btn-submit').removeAttr('disabled');
          }
        });
        return false;
      }
    }
  });

  // ========================================
  // Masks
  // ========================================
  $('.js-phone').mask('(00) 0000-0000');
  $('.js-cell-phone').mask('(00) 00000-0000');
  $('.js-date').mask('00/00/0000');
  $('.js-cpf').mask('000.000.000-00');
  $('.js-rg').mask('0.000.000');

});