<?php
/*
 * Request
 */

try{

	//Defining the header
	header( 'Content-type: application/json' );

	//Defining
	$return	= [ 'tipo' => 'sucesso' ];

	//Inserting
	InsertController::main( $page->module, $_POST, ( isset( $_FILES ) ) ? $_FILES : null );

}catch( Exception $e ){

	//Defining
	$return	= [ 'tipo' => 'erro', 'mensagem' => $e->getMessage() ];

}

//Returning
echo json_encode( $return );