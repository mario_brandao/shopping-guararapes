<?php
/**
 * Connection
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage connection
 * @version 1.0
 * @since 2018/04/10 18:34
 */
Class ConnectionDBR{

	/**
	 * Connection
	 *
	 * @access private
	 * @var PDO
	 */
	private $connection;

	/**
	 * Instance
	 *
	 * @access private
	 * @var PDO
	 */
	private static $instance = null;

	/**
	 * Constructor
	 *
	 * @access public
	 */
	public function __construct(){

		try{

			//Defining
			$this->connection	= new PDO( 'mysql:host=' . DB_ADDRESS . ';port=' . DB_PORT . ';dbname=' . DB_BASE . ';charset=utf8', DB_USER, DB_PASSWORD, array( PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true ) );

		}catch( PDOException $e ){

			//Redirecting
			header( 'Location: ' . BASE_URL . '/manutencao' );

		}

	}

	/**
	 * Defining the instance
	 *
	 * @access public
	 * @return ConnectionDBR
	 */
	public static function getInstance(){

		//Verificando se a instância já foi setada
		if( is_null( self::$instance ) )
			//Setando a isntância
			self::$instance	= new self();

		//Retornando a instância
		return self::$instance;

	}

	/**
	 * Getting the connection
	 *
	 * @access public
	 * @return PDO
	 */
	public function getConnection(){

		//Retornando a conexão
		return $this->connection;

	}

}