<?php
/**
 * Count class
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage count
 * @version 1.0
 * @since 2018/04/11 14:42
 */
Class CountController{

	/**
	 * Defining the repository
	 *
	 * @access public
	 * @static
	 * @param string $type Model's type
	 * @uses CountRepositoryDBR::getInstance Defining the instance
	 * @return mixed
	 */
	public static function getRepository( $type = null ){

		//Returning
		return CountRepositoryDBR::getInstance( $type );

	}

	/**
	 * Counting
	 *
	 * @access public
	 * @static
	 * @uses self::getRepository Getting the repository
	 * @uses CountRepositoryDBR->count Counting
	 * @return integer
	 */
	public static function count( $type = 'store' ){

		//Returning
		return self::getRepository( $type )->count();

	}

}