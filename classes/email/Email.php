<?php
/**
 * Email's class
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage email
 * @version 1.0
 * @since 1.0 2018/04/11 14:35
 */
Class Email{

	/**
	 * Recipient
	 *
	 * @access private
	 * @var string
	 */
	private $recipient;

	/**
	 * Subject
	 *
	 * @access private
	 * @var string
	 */
	private $subject;

	/**
	 * Message
	 *
	 * @access private
	 * @var string
	 */
	private $message;

	/**
	 * Return Path
	 *
	 * @access private
	 * @var string
	 */
	private $returnPath;

	/**
	 * Method constructor of the class
	 *
	 * @access public
	 * @param string $recipient Destinatário do e-mail
	 * @param string $subject Assunto do e-mail
	 * @param string $message Mensagem do e-mail
	 * @uses $this->send Sending
	 */
	public function __construct( $recipient = null, $subject = null, $message = null, $returnPath = EMAIL_RETURN_PATH ){

		$this->recipient	= $recipient;
		$this->subject		= $subject;
		$this->message		= $message;
		$this->returnPath	= $returnPath;
		$this->send();

	}

	/**
	 * Sending
	 *
	 * @access public
	 */
	public function send(){

		//Defining
		$email 				= new PHPMailer;

		//Configurating
		$email->isSMTP();
		$email->Host 		= EMAIL_SERVER;
		$email->SMTPAuth 	= true;
		$email->Username 	= EMAIL;
		$email->Password 	= EMAIL_PASSWORD;
		$email->SMTPSecure 	= EMAIL_SECURITY;
		$email->Port 		= EMAIL_PORT;
		$email->CharSet		= 'UTF-8';
		$email->Sender		= EMAIL;
		$email->isHTML( true );
		$email->setFrom( EMAIL, PROJECT_NAME );
		$email->addAddress( $this->recipient );

		//Checking
		if( !is_null( $this->returnPath ) )
			//Defining
			$email->ReturnPath	= $this->returnPath;

		//Defining
		$email->Subject = $this->subject;
		$email->Body    = $this->message;

		//Sending
		$email->send();

	}

}