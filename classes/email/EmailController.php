<?php
/**
 * Email's controller
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage email
 * @version 1.0
 * @since 1.0 2018/04/17 14:12
 */
Class EmailController{

	/**
	 * Sending the contact
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param integer $destination Destination
	 * @uses ListController::byDestination Listing by destination
	 * @uses EmailTemplate::contact Defining the contact's template
	 */
	public static function send( $id, $destination ){

		//Defining
		$records	= ListController::byDestination( $destination );

		//Checking
		if( !is_null( $records ) ){

			//Checking the destination
			switch( $destination ){

				//Contact
				case DESTINATION_CONTACT:

					//Defining the subject
					$subject	= 'Contato do fale conosco';

					//Defining the message
					$message	= EmailTemplate::contact( SearchController::byId( 'contact', $id ) );

				break;

			}

			//Listando
			foreach( $records as $contact )
				//Enviando o e-mail
				new Email( $contact->email, $subject, $message );

		}

	}

}