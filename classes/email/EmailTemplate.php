<?php
/**
 * Email's template
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage email
 * @version 1.0
 * @since 2018/04/11 15:15
 **/
Class EmailTemplate{

	/**
	 * Defining the contact's template
	 *
	 * @access public
	 * @static
	 * @param array $fields Fields
	 * @return string
	 */
	public static function contact( stdclass $object ){

		//Defining the template
		$template				= new Template( BASE_DIR_TEMPLATE . '/email/contact.html', false, false );

		//Defining some variables
		$template->OBJ			= $object;
		$template->BASE_URL_IMG	= BASE_URL_IMG;
		$template->PROJECT_NAME	= PROJECT_NAME;
		$template->SYSTEM_URL	= SYSTEM_URL;
		$template->SYSTEM_NAME	= SYSTEM_NAME;
		$template->CURRENT_YEAR	= date( 'Y' );

		//Returning
		return $template->parse();

	}

}