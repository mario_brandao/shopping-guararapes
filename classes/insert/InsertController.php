<?php
/**
 * Insert class
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage insert
 * @version 1.0
 * @since 2018/04/11 11:58
 */
Class InsertController{

	/**
	 * Defining the repository
	 *
	 * @access public
	 * @static
	 * @uses InsertRepositoryDBR::getInstance Defining the instance
	 * @return mixed
	 */
	public static function getRepository(){

		//Retornando
		return InsertRepositoryDBR::getInstance();

	}

	/**
	 * Main method
	 *
	 * @access public
	 * @static
	 * @param string $form Form
	 * @param array $fields Fields
	 * @param array $upload Upload file
	 * @uses Util::formatFields Formating the fields
	 * @uses Validation::form Validating the form
	 * @uses self::contact Adding the contact
	 * @uses self::franchise Adding the franchise
	 * @uses self::dealer Adding the dealer
	 * @uses self::event Adding the event
	 */
	public static function main( $form = null, $fields = [], $upload = null ){

		//Formating
		$fields	= Util::formatFields( $fields );

		//Validating the fields
		Validation::form( $form, $fields, $upload );

		//Checking
		switch( $form ){

			//Contact
			case 'contact':

				//Adding
				self::contact( $fields );

			break;

		}

	}

	/**
	 * Inserting the contact
	 *
	 * @access public
	 * @static
	 * @param array $fields
	 * @uses self::getRepository Getting the repository
	 * @uses InsertRepositoryDBR->contact Inserting the contact
	 * @uses EmailController::send Sending
	 */
	public static function contact( $fields = [] ){

		//Inserting and defining
		$id	= self::getRepository()->contact( $fields );

		//Sending
		EmailController::send( $id, DESTINATION_CONTACT );

	}

}