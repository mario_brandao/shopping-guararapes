<?php
/**
 * Insert repository
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage insert
 * @version 1.0
 * @since 2018/04/09 19:25
 */
Class InsertRepositoryDBR{

	/**
	 * Connection's object
	 *
	 * @access private
	 * @var PDO
	 */
	private $connection;

	/**
	 * Showing or not the exception
	 *
	 * @access private
	 * @var boolean
	 */
	private $exception	= true;

	/**
	 * Instance
	 *
	 * @access private
	 * @static
	 * @var ListRepositoryDBR
	 */
	private static $instance = null;

	/**
	 * Class constructor
	 *
	 * @access public
	 * @uses ConnectionDBR::getInstance Defining the instance
	 */
	public function __construct(){

		$this->connection	= ConnectionDBR::getInstance()->getConnection();

	}

	/**
	 * Defining the instance
	 *
	 * @access public
	 * @return InsertRepositoryDBR
	 */
	public static function getInstance(){

		//Checking
		if( is_null( self::$instance ) )
			//Defining
			self::$instance	= new self();

		//Returning
		return self::$instance;

	}

	/**
	 * Inserting the contact
	 *
	 * @access public
	 * @param array $fields Fields
	 * @throws Exception
	 * @return integer
	 */
	public function contact( $fields ){

		try{

			//Preparing
			$sql	= $this->connection->prepare( 'INSERT INTO contact(source,subject,name,email,phone,message,date) VALUES(:source,:subject,:name,:email,:phone,:message,NOW())' );

			//Defining
			$sql->bindValue( ':source', SOURCE_SITE_INTERNAL, PDO::PARAM_INT );
			$sql->bindValue( ':subject', $fields[ 'assunto' ] );
			$sql->bindValue( ':name', $fields[ 'nome' ] );
			$sql->bindValue( ':email', $fields[ 'email' ] );
			$sql->bindValue( ':phone', $fields[ 'telefone' ] );
			$sql->bindValue( ':message', $fields[ 'mensagem' ] );

			//Executing
			$sql->execute();

			//Returning
			return $this->connection->lastInsertId();

		}catch( PDOException $e ){

			//Throwing the exception
			throw new Exception( 'Ocorreu um erro durante a inserção do contato do fale conosco. Por favor, tente novamente mais tarde.' );

		}

	}

}