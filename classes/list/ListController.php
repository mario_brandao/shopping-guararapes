<?php
/**
 * List class
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage list
 * @version 1.0
 * @since 05/02/2015 16:58
 */
Class ListController{

	/**
	 * Defining the repository
	 *
	 * @access public
	 * @static
	 * @param string $type Model's type
	 * @param string $instance Instance's type
	 * @uses ListRepositoryDBR::getInstance Defining the instance
	 * @uses ListRepositoryAPI::getInstance Defining the instance
	 * @return mixed
	 */
	public static function getRepository( $type = null, $instance = null ){

		//Returning
		return ( is_null( $instance ) ) ? ListRepositoryDBR::getInstance( $type ) : ListRepositoryAPI::getInstance();

	}

	/**
	 * Listing orderly
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param string $order Order
	 * @param boolean $exception Showing or not the exception
	 * @uses self::getRepository Defining the repository
	 * @uses ListRepositoryDBR->orderly Listing orderly
	 * @return mixed
	 */
	public static function orderly( $type = 'banner', $order = null, $exception = true ){

		//Defining
		$repository	= self::getRepository( $type );

		//Showing or not the exception
		$repository->exception = $exception;

		//Returning
		return $repository->orderly( $order );

	}

	/**
	 * Listing orderly and limited
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param string $order Order
	 * @param integer $limit Limit
	 * @param boolean $exception Showing or not the exception
	 * @uses Validation::numericFields Validating the numeric fields
	 * @uses self::getRepository Defining the repository
	 * @uses ListRepositoryDBR->orderlyLimited Listing orderly and limited
	 * @return mixed
	 */
	public static function orderlyLimited( $type = 'store', $order = null, $limit = 0, $exception = true ){

		//Validating
		Validation::numericFields( [ 'quantidade' => $limit ] );

		//Defining
		$repository	= self::getRepository( $type );

		//Showing or not the exception
		$repository->exception = $exception;

		//Returning
		return $repository->orderlyLimited( $order, $limit );

	}

	/**
	 * Listing related by your content and orderly
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param integer $content Content
	 * @param string $order Order
	 * @param boolean $exception Showing or not the exception
	 * @uses Validation::numericFields Validating the numeric fields
	 * @uses ListRepositoryDBR->relatedByContentOrderly Lsting related by your content and orderly
	 * @return mixed
	 */
	public static function relatedByContentOrderly( $type = null, $content = null, $order = null, $exception = false ){

		//Validating
		Validation::numericFields( [ $type => $content ] );

		//Defining
		$repository				= self::getRepository( $type );

		//Showing or not the exception
		$repository->exception 	= $exception;

		//Defining
		$records				= $repository->relatedByContentOrderly( $content, $order );

		//Returning
		return $records;

	}

	/**
	 * Listing the not active, by your identifier, orderly and by quantity
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param integer $id Identifier
	 * @param string $order Order
	 * @uses Validation::numericFields Checking the numeric fields
	 * @uses self::getRepository Defining the repository
	 * @uses ListRepositoryDBR->notActiveByIdOrderlyByQuantity Listing the not active, by your identifier, orderly and by quantity
	 * @return mixed
	 */
	public static function notActiveByIdOrderlyByQuantity( $type = null, $id = null, $order = null, $quantity = 10, $exception = false ){

		//Validating
		Validation::numericFields( [ 'identificador' => $id ] );

		//Defining
		$repository	= self::getRepository( $type );

		//Showing or not the exception
		$repository->exception	= $exception;

		//Defining
		$records	= $repository->notActiveByIdOrderlyByQuantity( $id, $order, $quantity );

		//Returning
		return $records;

	}

	/**
	 * Listing the not active, by your identifier, by type, orderly and by quantity
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param integer $id Identifier
	 * @param integer $contentType Content's type
	 * @param string $order Order
	 * @uses Validation::numericFields Checking the numeric fields
	 * @uses self::getRepository Defining the repository
	 * @uses ListRepositoryDBR->notActiveByIdByTypeOrderlyByQuantity Listing the not active, by your identifier, by type, orderly and by quantity
	 * @return mixed
	 */
	public static function notActiveByIdByTypeOrderlyByQuantity( $type = null, $id = null, $contentType = null, $order = null, $quantity = 10, $exception = false ){

		//Validating
		Validation::numericFields( [ 'identificador' => $id, 'tipo do conteúdo' => $contentType ] );

		//Defining
		$repository	= self::getRepository( $type );

		//Showing or not the exception
		$repository->exception	= $exception;

		//Defining
		$records	= $repository->notActiveByIdByTypeOrderlyByQuantity( $id, $contentType, $order, $quantity );

		//Returning
		return $records;

	}

	/**
	 * Listing by destination
	 *
	 * @access public
	 * @param integer $destination Destination
	 * @uses Validation::numericFields Validating the numeric fields
	 * @uses self::getRepository Defining the repository
	 * @uses ListRepositoryDBR->byDestination Listing by destination
	 * @return mixed
	 */
	public static function byDestination( $destination = null ){

		//Validating
		Validation::numericFields( [ 'destino' => $destination ] );

		//Returning
		return self::getRepository()->byDestination( $destination );

	}

	/**
	 * Importing the cinema
	 *
	 * @access public
	 * @static
	 * @uses self::importCinemaList Importing the cinema's list
	 * @uses self::importCinemaMovie Importing the cinema's movies
	 */
	public static function importCinema(){

		//Importing the lists
		self::importCinemaList();

		//Importing the movies
		self::importCinemaMovie();

	}

	/**
	 * Importing the cinema's list
	 *
	 * @access public
	 * @static
	 * @uses self::getRepository Getting the repository
	 * @uses ListRepositoryAPI->list Listing
	 */
	public static function importCinemaList(){

		//Creating the lists
		self::getRepository( null, 'API' )->list();

	}

	/**
	 * Importing the cinema's movie
	 *
	 * @access public
	 * @static
	 * @uses SearchController::movie Searching the movie
	 */
	public static function importCinemaMovie(){

		//Defining
		$json	= json_decode( file_get_contents( BASE_DIR_REPOSITORY . '/json/cinema/list.json' ) );

		//Checking
		if( isset( $json ) && count( $json->movies ) > 0 )
			//Listing
			foreach( $json->movies as $movie )
				//Creating
				SearchController::movie( $movie->id, $movie->url );

	}

}