<?php
/**
 * List repository
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage list
 * @version 1.0
 * @since 2018/04/09 19:25
 */
Class ListRepositoryAPI{

	/**
	 * Instance
	 *
	 * @access private
	 * @static
	 * @var ListRepositoryAPI
	 */
	private static $instance = null;

	/**
	 * Defining the instance
	 *
	 * @access public
	 * @return ListRepositoryAPI
	 */
	public static function getInstance(){

		//Checking
		if( is_null( self::$instance ) )
			//Defining
			self::$instance	= new self();

		//Returning
		return self::$instance;

	}

	/**
	 * Creating the list's json
	 *
	 * @access public
	 * @uses Util::formatStringUrl Formatting the string to URL format
	 */
	public function list(){

		//Defining
		$days	= json_decode( file_get_contents( INGRESSO_COM_API_URL . '/sessions/city/' . INGRESSO_COM_API_CITY . '/theater/' . INGRESSO_COM_API_THEATER . '/partnership/' . INGRESSO_COM_API_PARTERSHIP ) );

		//Defining
		$array	= [];

		//Listing the movies
		foreach( $days[ 0 ]->movies as $movie ){

			//Defining
			$genres		= [];
			$rooms		= [];

			//Listing
			foreach( $movie->genres as $genre )
				//Defining
				$genres[]	= trim( $genre );

			//Listing
			foreach( $movie->rooms as $room ){

				//Defining
				$sessions	= [];

				//Listing
				foreach( $room->sessions as $session ){

					//Defining
					$types	= [];

					//Listing
					foreach( $session->type as $type )
						//Defining
						$types[]	= $type;

					//Defining
					$sessions[]	= [ 'types' => $types, 'time' => $session->time ];

				}

				//Defining
				$rooms[]	= [ 'name' => $room->name, 'sessions' => $sessions ];

			}

			//Defining
			$array[]	= [ 'id' => $movie->id, 'url' => trim( $movie->siteURL ), 'title' => trim( $movie->title ), 'slug' => Util::formatStringUrl( trim( $movie->title ) ), 'pre_sale' => $movie->inPreSale, 'poster' => $movie->images[ 0 ]->url, 'rating' => trim( $movie->contentRating ), 'genres' => implode( ', ', $genres ), 'rooms' => $rooms ];

		}

		//Checking
		if( count( $array ) > 0 )
			//Saving
			file_put_contents( BASE_DIR_REPOSITORY . '/json/cinema/list.json', json_encode( [ 'created_at' => date( 'Y-m-d H:i:s' ), 'date' => $days[ 0 ]->date , 'movies' => $array ] ) );

	}

}