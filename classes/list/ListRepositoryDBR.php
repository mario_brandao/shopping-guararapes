<?php
/**
 * List repository
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage list
 * @version 1.0
 * @since 2018/04/09 19:25
 */
Class ListRepositoryDBR{

	/**
	 * Connection's object
	 *
	 * @access private
	 * @var PDO
	 */
	private $connection;

	/**
	 * Showing or not the exception
	 *
	 * @access private
	 * @var boolean
	 */
	public $exception	= true;

	/**
	 * Instance
	 *
	 * @access private
	 * @static
	 * @var ListRepositoryDBR
	 */
	private static $instance = [];

	/**
	 * Class constructor
	 *
	 * @access public
	 * @param string $type Type
	 * @uses ConnectionDBR::getInstance Defining the instance
	 */
	public function __construct( $type ){

		$this->connection	= ConnectionDBR::getInstance()->getConnection();
		$this->type			= $type;

	}

	/**
	 * Getting the instance
	 *
	 * @access public
	 * @param string $type Type
	 * @return ConnectionDBR
	 */
	public static function getInstance( $type ){

		//Checking
		if( !array_key_exists( $type, self::$instance ) )
			//Defining
			self::$instance[ $type ]	= new self( $type );

		//Returning
		return self::$instance[ $type ];

	}

	/**
	 * Listing by destination
	 *
	 * @access public
	 * @param integer $destination Destination
	 * @param string $complement Complement
	 * @param array $params Params
	 * @throws Exception
	 * @return mixed
	 */
	public function byDestination( $destination, $complement = null, $params = [] ){

		try{

			//Preparing
			$resource	= $this->connection->prepare( 'SELECT DISTINCT(R.email) AS email FROM recipient R INNER JOIN recipient_company RC ON (R.id = RC.recipient) INNER JOIN recipient_destination RD ON (R.id = RD.recipient) WHERE RD.destination = :destination ' . $complement );

			//Checking
			if( count( $params ) > 0 )
				//Listing
				foreach( $params as $field => $configuration )
					//Defining
					$resource->bindValue( $field, $configuration[ 'value' ], $configuration[ 'type' ] );

			//Defining
			$resource->bindValue( ':destination', $destination, PDO::PARAM_INT );

			//Executing
			$resource->execute();

			//Defining
			$records	= $resource->fetchAll( PDO::FETCH_OBJ );

			//Checking
			if( count( $records ) > 0 ){

				//Listring
				foreach( $records as $record )
					//Defining
					$array[]	= $record;

				//Returning
				return $array;

			}else
				//Returning
				return null;

		}catch( PDOException $e ){

			//Throwing the exception
			throw new Exception( 'Ocorreu um erro durante a listagem dos contatos por destino. Por favor, tente novamente mais tarde.' );

		}

	}

	/**
	 * Listing related by your content and orderly
	 *
	 * @access public
	 * @param integer $content Conteúdo
	 * @param string $order Order
	 * @uses Util::defineDatabaseOrder Defining the database's order
	 * @throws Exception
	 * @return mixed
	 */
	public function relatedByContentOrderly( $content, $order ){

		try{

			//Checking
			switch( $this->type ){

				//Categories
				case 'category-by-type':

					//Defining
					$sql	= 'SELECT * FROM store_category WHERE type = :id ORDER BY ' . Util::defineDatabaseOrder( $order );

				break;

				//Institutional's gallery
				case 'institutional-gallery':

					//Defining
					$sql	= 'SELECT * FROM institutional_image WHERE institutional = :id ORDER BY ' . Util::defineDatabaseOrder( $order );

				break;

			}

			//Preparing
			$resource	= $this->connection->prepare( $sql );

			//Defining
			$resource->bindValue( ':id', $content, PDO::PARAM_INT );

			//Executing
			$resource->execute();

			//Defining
			$records	= $resource->fetchAll( PDO::FETCH_OBJ );

			//Checking the quantity
			if( count( $records ) > 0 )
				//Returning
				return $records;
			else
				//Returning
				return null;

		}catch( PDOException $e ){

			//Throwing the exception
			throw new Exception( 'Ocorreu um erro durante a listagem dos conteúdos relacionados. Por favor, tente novamente mais tarde.' );

		}

	}

	/**
	 * Listing
	 *
	 * @access public
	 * @param string $complement Complement
	 * @param array $params PArams
	 * @throws Exception
	 * @return stdclass[]
	 */
	public function list( $complement = null, $params = [] ){

		try{

			//Checking
			switch( $this->type ){

				//Stores by categorie's type
				case 'store-by-category-by-type':

					//Defining
					$sql	= 'SELECT TBL.* FROM store TBL INNER JOIN store_category TBL2 ON (TBL.category = TBL2.id) WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Subject
				case 'subject':

					//Defining
					$sql	= 'SELECT TBL.* FROM subject TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Store
				case 'store':

					//Defining
					$sql	= 'SELECT TBL.* FROM store TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Servive
				case 'service':

					//Defining
					$sql	= 'SELECT TBL.* FROM service TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Servive
				case 'sustainability':

					//Defining
					$sql	= 'SELECT TBL.* FROM sustainability TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//News
				case 'news':

					//Defining
					$sql	= 'SELECT TBL.* FROM news TBL WHERE date(date) <= NOW() ' . $complement;

				break;

				//Banner
				default:

					//Defining
					$sql	= 'SELECT * FROM banner TBL WHERE TBL.status = TRUE ' . $complement;

				break;

			}

			//Preparing
			$resource	= $this->connection->prepare( $sql );

			//Checking
			if( count( $params ) > 0 )
				//Listing
				foreach( $params as $field => $config )
					//Defining
					$resource->bindValue( $field, $config[ 'value' ], $config[ 'type' ] );

			//Executing
			$resource->execute();

			//Defining
			$records	= $resource->fetchAll( PDO::FETCH_OBJ );

			//Checking
			if( count( $records ) > 0 )
				//Returning
				return $records;
			//Checking
			else if( $this->exception )
				//Throwing the exception
				throw new Exception( 'Nenhum registro encontrado.' );
			else
				//Returning
				return null;

		}catch( PDOException $e ){

			//Throwing the exception
			throw new Exception( 'Ocorreu um erro durante a listagem. Por favor, tente novamente mais tarde.' );

		}

	}

	/**
	 * Listing orderly and limited
	 *
	 * @access public
	 * @param string $order Order
	 * @param integer $limit Limit
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses $this->orderly Listing orderly
	 * @return mixed
	 */
	public function orderlyLimited( $order, $limit, $complement = null, $params = [] ){

		//Defining
		$complement	= 'LIMIT :limit, ' . RECORDS_PER_PAGE . ' ' . $complement;

		//Definindo os parâmetros
		$params		= array_merge( [ ':limit' => [ 'value' => $limit, 'type' => PDO::PARAM_INT ] ], $params );

		//Listing
		return $this->orderly( $order, $complement, $params );

	}

	/**
	 * Listing orderly
	 *
	 * @access public
	 * @param string $order Order
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses Util::defineDatabaseOrder Defining the database order
	 * @uses $this->list Listing
	 * @return mixed
	 */
	public function orderly( $order, $complement = null, $params = [] ){

		//Defining
		$complement	= 'ORDER BY ' . Util::defineDatabaseOrder( $order ) . ' ' . $complement;

		//Listing
		return $this->list( $complement, $params );

	}

	/**
	 * Listing the not active by your identifier, by type, orderly and by quantity
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param integer $type Type
	 * @param string string $order Order
	 * @param integer $quantity Quantity
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses $this->notActiveByIdByTypeOrderly Listing the not active, by your identifier, by type and orderly
	 * @return mixed
	 */
	public function notActiveByIdByTypeOrderlyByQuantity( $id, $type, $order, $quantity, $complement = null, $params = [] ){

		//Defining
		$complement	= 'LIMIT :quantity ' . $complement;

		//Defining
		$params	= array_merge( [ ':quantity' => [ 'value' => $quantity, 'type' => PDO::PARAM_INT ] ], $params );

		//Listing
		return $this->notActiveByIdByTypeOrderly( $id, $type, $order, $complement, $params );

	}

	/**
	 * Listing the not active by your identifier, by type and orderly
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param integer $type Type
	 * @param string string $order Order
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses Util::defineDatabaseOrder Defining the database order
	 * @uses $this->notActiveByIdByType Listing the not active by your identifier and by type
	 * @return mixed
	 */
	public function notActiveByIdByTypeOrderly( $id, $type, $order, $complement = null, $params = [] ){

		//Defining
		$complement	= 'ORDER BY ' . Util::defineDatabaseOrder( $order ) . ' ' . $complement;

		//Listing
		return $this->notActiveByIdByType( $id, $type, $complement, $params );

	}

	/**
	 * Listing the not active by your identifier and by type
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param integer $type Type
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses $this->notActiveById Listing the not active by your identifier
	 * @return mixed
	 */
	public function notActiveByIdByType( $id, $type, $complement = null, $params = [] ){

		//Defining
		$complement	= 'AND TBL2.type = :type ' . $complement;

		//Defining
		$params	= array_merge( [ ':type' => [ 'value' => $type, 'type' => PDO::PARAM_INT ] ], $params );

		//Listing
		return $this->notActiveById( $id, $complement, $params );

	}

	/**
	 * Listing the not active by your identifier, orderly and by quantity
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param string string $order Order
	 * @param integer $quantity Quantity
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses $this->notActiveByIdOrderly Listing the not active, by your identifier and orderly
	 * @return mixed
	 */
	public function notActiveByIdOrderlyByQuantity( $id, $order, $quantity, $complement = null, $params = [] ){

		//Defining
		$complement	= 'LIMIT :quantity ' . $complement;

		//Defining
		$params	= array_merge( [ ':quantity' => [ 'value' => $quantity, 'type' => PDO::PARAM_INT ] ], $params );

		//Listing
		return $this->notActiveByIdOrderly( $id, $order, $complement, $params );

	}

	/**
	 * Listing the not active by your identifier and orderly
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param string string $order Order
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses Util::defineDatabaseOrder Defining the database order
	 * @uses $this->notActiveById Listing the not active by your identifier
	 * @return mixed
	 */
	public function notActiveByIdOrderly( $id, $order, $complement = null, $params = [] ){

		//Defining
		$complement	= 'ORDER BY ' . Util::defineDatabaseOrder( $order ) . ' ' . $complement;

		//Listing
		return $this->notActiveById( $id, $complement, $params );

	}

	/**
	 * Listing the not active by your identifier
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param string $complement Complement
	 * @param array $params Params
	 * @uses $this->list Listing
	 * @return mixed
	 */
	public function notActiveById( $id, $complement = null, $params = [] ){

		//Defining
		$complement	= 'AND TBL.id <> :id ' . $complement;

		//Defining
		$params	= array_merge( [ ':id' => [ 'value' => $id, 'type' => PDO::PARAM_INT ] ], $params );

		//Listing
		return $this->list( $complement, $params );

	}

}