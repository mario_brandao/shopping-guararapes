<?php
/**
 * Page's class
 *
 * @author Lucas Dantas <lucas.dantas@gmail.com>
 * @package util
 * @subpackage page
 * @version 1.0
 * @since 1.0 2016/10/10 20:52
 */
Class Page{

	/**
	 * Module
	 *
	 * @access public
	 * @var string
	 */
	public $module;

	/**
	 * Action
	 *
	 * @access public
	 * @var string
	 */
	public $action;

	/**
	 * Identifier
	 *
	 * @access private
	 * @var integer
	 */
	private $id;

	/**
	 * Page's type
	 *
	 * @access private
	 * @var string
	 */
	private $type;

	/**
	 * Page for pagination
	 *
	 * @access private
	 * @var integer
	 */
	private $page;

	/**
	 * Slug
	 *
	 * @access private
	 * @var string
	 */
	private $slug;

	/**
	 * Model
	 *
	 * @access private
	 * @var string
	 */
	private $model;

	/**
	 * Search's term
	 *
	 * @access private
	 * @var string
	 */
	private $term;

	/**
	 * Modules
	 *
	 * @access public
	 * @var array
	 */
	public $modules;

	/**
	 * Constructor
	 *
	 * @access public
	 * @param array $queryString Query string
	 * @uses $this->formatQueryString Formatting the query string
	 * @uses $this->dispatch Showing the page
	 */
	public function __construct( $queryString = null ){

		$this->formatQueryString( $queryString );
		$this->dispatch();

	}

	/**
	 * Formatting the query's string
	 *
	 * @access private
	 * @param array $queryString Query string
	 */
	private function formatQueryString( $queryString ){

		//Checking if exists
		if( array_key_exists( 'module', $queryString ) )
			//Defining
			$this->setModule( $queryString[ 'module' ] );

		//Checking if exists
		if( array_key_exists( 'id', $queryString ) )
			//Defining
			$this->id	= $queryString[ 'id' ];

		//Checking if exists
		if( array_key_exists( 'type', $queryString ) )
			//Defining
			$this->type	= $queryString[ 'type' ];

		//Checking if exists
		if( array_key_exists( 'page', $queryString ) )
			//Defining
			$this->page	= $queryString[ 'page' ];

		//Checking if exists
		if( array_key_exists( 'slug', $queryString ) )
			//Defining
			$this->slug	= $queryString[ 'slug' ];

		//Defining
		$this->action	= ( array_key_exists( 'action', $queryString ) ) ? $queryString[ 'action' ] : 'index';

	}

	/**
	 * Setting the module
	 *
	 * @access public
	 * @param string $module
	 * @uses Factory::getController Getting the controller
	 * @uses Controller->searchByTranslate Searching by translate
	 */
	public function setModule( $module ){

		//Defining
		$this->module	= $module;

	}

	/**
	 * Dispatching
	 *
	 * @access private
	 */
	private function dispatch(){

		//Defining some variables
		$page	= $this;

		//Checking the type
		switch( $this->type ){

			//Treatment
			case 'treatment':

				//Checking
				if( !is_null( $this->action ) )
					//Defining
					$path	= BASE_DIR_ASSETS . '/submit/' . $this->action . '.php';

			break;

			//Default
			default:

				//Checking
				if( !is_null( $this->module ) )
					//Checking
					if( !is_null( $this->action ) )
						//Defining
						$path	= BASE_DIR_ASSETS . '/php/' . $this->module . '/' . $this->action . '.php';
					else
						//Defining
						$path	= BASE_DIR_ASSETS . '/php/' . $this->module . '/index.php';

			break;

		}

		//Checking
		if( !is_null( $path ) ){

			//Formatting the path
			$path	= realpath( $path );

			//Checking
			if( file_exists( $path ) )
				//Including
				require_once( $path );
			else
				//Redirecting
				header( 'Location:' . BASE_URL . '/404' );

		}else
			//Redirecting
			header( 'Location:' . BASE_URL . '/404' );

	}

}