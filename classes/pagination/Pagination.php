<?php
/**
 * Pagination's class
 *
 * @author Rafael Cardoso <rafus0@hotmail.com>
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage util
 * @version 1.0
 * @since 1.0 2017/04/25 19:38
 */
Class Pagination{

	/**
	 * Offset
	 *
	 * @access private
	 * @var int
	 */
	private $offset;

	/**
	 * Record's per page
	 *
	 * @access private
	 * @var int
	 */
	private $recordsPerPage;

	/**
	 * Record's quantity
	 *
	 * @access private
	 * @var int
	 */
	private $quantity;

	/**
	 * First page
	 *
	 * @access private
	 * @var int
	 */
	private $firstPage;

	/**
	 * Page's quantity
	 *
	 * @access private
	 * @var int
	 */
	private $pageQuantity;

	/**
	 * Function to javascript
	 *
	 * @access private
	 * @var string
	 */
	private $module;

	/**
	 * Search's term
	 *
	 * @access private
	 * @var string
	 */
	private $searchTerm;

	/**
	 * Pagination
	 *
	 * @access private
	 * @var string
	 */
	public $pagination;

	/**
	 * Next page
	 *
	 * @access private
	 * @var integer
	 */
	private $nextPage;

	/**
	 * Previous page
	 *
	 * @access private
	 * @var integer
	 */
	private $previousPage;

	/**
	 * Class constructor method
	 *
	 * @access public
	 * @param integer $offset Offset
	 * @param integer $recordsPerPage Records per page
	 * @param integer $quantity Record's quantity
	 * @param string $module Module
	 * @param string $searchTerm Search's term
	 * @uses $this->setOffset Defining the offset
	 * @uses $this->setRecordsPerPage Defining the records per page
	 * @uses $this->setQuantity Defining the quantity
	 * @uses $this->setModule Defining the module
	 * @uses $this->setSearchTerm Defining the search's term
	 * @uses $this->setNextPage Defining the next page
	 * @uses $this->setPreviousPage Defining the previous page
	 */
	public function __construct( $offset = 0, $recordsPerPage = RECORDS_PER_PAGE, $quantity = 0, $module = null, $searchTerm = null ){

		$this->setOffset( ceil( $offset / $recordsPerPage) + 1 );
		$this->setRecordsPerPage( $recordsPerPage );
		$this->setQuantity( $quantity );
		$this->setModule( $module );
		$this->setSearchTerm( $searchTerm );
		$this->setNextPage( ( $this->getOffset() + 1 ) );
		$this->setPreviousPage( ( $this->getOffset() - 1 ) );

		//Checking
		if( $this->getOffset() > 8 )
			//Checking
			if ( $this->getOffset() > 1 )
				//Checking
				if( $this->getOffset() > 4 )
					//Defining
					$this->setFirstPage( $this->getOffset() - 4 );
				else
					//Defining
					$this->setFirstPage( 1 );
			else
				//Defining
				$this->setFirstPage( $this->getOffset() );
		else
			//Defining
			$this->setFirstPage( 1 );

		//Paginating
		$this->paginate();

	}

	/**
	 * Defining the value of <var>$this->intOffset</var>
	 *
	 * @access public
	 * @param integer $offset
	 */
	public function setOffset( $offset ){

		$this->intOffset	= str_pad( $offset, 2, '0', STR_PAD_LEFT );

	}

	/**
	 * Return the value of <var>$this->intOffset</var>
	 *
	 * @access public
	 * @return <var>$this->intOffset</var>
	 */
	public function getOffset(){

		return $this->intOffset;

	}

	/**
	 * Defining the value of <var>$this->recordsPerPage</var>
	 *
	 * @access public
	 * @param string $recordsPerPage
	 */
	public function setRecordsPerPage( $recordsPerPage ){

		$this->recordsPerPage	= $recordsPerPage;

	}

	/**
	 * Return the value of <var>$this->recordsPerPage</var>
	 *
	 * @access public
	 * @return <var>$this->recordsPerPage</var>
	 */
	public function getRecordsPerPage(){

		return $this->recordsPerPage;

	}

	/**
	 * Defining the value of <var>$this->quantity</var>
	 *
	 * @access public
	 * @param integer $quantity
	 */
	public function setQuantity( $quantity ){

		$this->quantity	= $quantity;

	}

	/**
	 * Return the value of <var>$this->quantity</var>
	 *
	 * @access public
	 * @return <var>$this->quantity</var>
	 */
	public function getQuantity(){

		return $this->quantity;

	}

	/**
	 * Defining the value of <var>$this->firstPage</var>
	 *
	 * @access public
	 * @param integer $firstPage
	 */
	public function setFirstPage( $firstPage ){

		$this->firstPage	= $firstPage;

	}

	/**
	 * Return the value of <var>$this->firstPage</var>
	 *
	 * @access public
	 * @return <var>$this->firstPage</var>
	 */
	public function getFirstPlace(){

		return $this->firstPage;

	}

	/**
	 * Defining the value of <var>$this->pageQuantity</var>
	 *
	 * @access public
	 * @param integer $pageQuantity
	 */
	public function setPageQuantity( $pageQuantity ){

		$this->pageQuantity	= str_pad( $pageQuantity, 2, '0', STR_PAD_LEFT );

	}

	/**
	 * Return the value of <var>$this->pageQuantity</var>
	 *
	 * @access public
	 * @return <var>$this->pageQuantity</var>
	 */
	public function getPageQuantity(){

		return $this->pageQuantity;

	}

	/**
	 * Defining the value of <var>$this->module</var>
	 *
	 * @access public
	 * @param string $module
	 */
	public function setModule( $module ){

		$this->module	= ( !is_null( $module ) ) ? '/' . $module : null;

	}

	/**
	 * Return the value of <var>$this->module</var>
	 *
	 * @access public
	 * @return <var>$this->module</var>
	 */
	public function getModule(){

		return $this->module;

	}

	/**
	 * Defining the value of <var>$this->searchTerm</var>
	 *
	 * @access public
	 * @param string $searchTerm
	 */
	public function setSearchTerm( $searchTerm ){

		$this->searchTerm	= ( strpos( $searchTerm, '?de=' ) === false ) ? ( !is_null( $searchTerm ) ? '?' . $searchTerm : null ) : $searchTerm;

	}

	/**
	 * Return the value of <var>$this->searchTerm</var>
	 *
	 * @access public
	 * @return string
	 */
	public function getSearchTerm(){

		return $this->searchTerm;

	}

	/**
	 * Defining the value of <var>$this->pagination</var>
	 *
	 * @access public
	 * @param string $pagination
	 */
	public function setPagination( $pagination ){

		$this->pagination	= $pagination;

	}

	/**
	 * Return the value of <var>$this->pagination</var>
	 *
	 * @access public
	 * @return <var>$this->pagination</var>
	 */
	public function getPagination(){

		return $this->pagination;

	}

	/**
	 * Defining the value of <var>$this->nextPage</var>
	 *
	 * @access public
	 * @param integer $nextPage
	 */
	public function setNextPage( $nextPage ){

		$this->nextPage	= $nextPage;

	}

	/**
	 * Return the value of <var>$this->nextPage</var>
	 *
	 * @access public
	 * @return integer
	 */
	public function getNextPage(){

		return $this->nextPage;

	}

	/**
	 * Defining the value of <var>$this->previousPage</var>
	 *
	 * @access public
	 * @param integer $previousPage
	 */
	public function setPreviousPage( $previousPage ){

		$this->previousPage	= $previousPage;

	}

	/**
	 * Return the value of <var>$this->previousPage</var>
	 *
	 * @access public
	 * @return integer
	 */
	public function getPreviousPage(){

		return $this->previousPage;

	}

	/**
	 * Paginating
	 *
	 * @access public
	 * @return string
	 * @uses $this->setPagination Defining the pagination
	 */
	public function paginate(){

		//Defining
		$pageNumber	= 0;

		//Listing
		while( $this->getQuantity() > 0 ){

		    //Increasing
			$pageNumber++;

			//Defining
		    $this->setQuantity( $this->getQuantity() - $this->getRecordsPerPage() );

		}

		//Defining
		$this->setPageQuantity( $pageNumber );

		//Checking
		if( $this->getQuantity() > 8 && ( $this->getOffset() - $this->getFirstPlace() ) > 2 )
			//Defining
			$this->setFirstPage( ( $this->getOffset() - $this->getFirstPlace() ) - 2 );

		//Defining
		$lastPage	= ( ( $this->getFirstPlace() + 8 ) < $pageNumber ) ? $this->getFirstPlace() + 8 : $pageNumber;

		//Defining
		$html		= null;

		//Checking
		if( $this->getOffset() != 1 )
			//Defining
			$html	.=  '<a href="' . BASE_URL . $this->getModule() . '/pagina/' . $this->getPreviousPage() . $this->getSearchTerm() . '" class="btn-prev"><i class="icon i-arrow"></i></a>';

		//Defining
		$html	.= '<select name="pagination" id="pagination" class="default-select pagination-select js-select-nav">';

		//Listing
		for( $i = $this->getFirstPlace() ; $i <= $lastPage; $i = $i + 1 ){

			//Checking
	    	if( $i != $this->getOffset() )
	    		//Defining
        		$html	.= '<option value="' . BASE_URL . $this->getModule() . '/pagina/' . $i . $this->getSearchTerm() . '">' . str_pad( $i, 2, '0', STR_PAD_LEFT ) . '</option>';
		    else
		    	//Defining
				$html .= '<option selected disabled>'. str_pad( $i, 2, '0', STR_PAD_LEFT ) .'</option>';

		}

		//Defining
		$html	.= '</select>';

		//Checking
		if( ( $lastPage != $this->getOffset() ) && ( $pageNumber != 0 ) )
			//Defining
			$html	.=  '<a href="' . BASE_URL . $this->getModule() . '/pagina/' . $this->getNextPage() . $this->getSearchTerm() . '" class="btn-next"><i class="icon i-arrow"></i></a>';

		//Defining
		$this->setPagination( $html );

	}

}