<?php
/**
 * Search class
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage search
 * @version 1.0
 * @since 2018/04/11 14:42
 */
Class SearchController{

	/**
	 * Defining the repository
	 *
	 * @access public
	 * @static
	 * @param string $type Model's type
	 * @param string $instance Instance's type
	 * @uses SearchRepositoryDBR::getInstance Defining the instance
	 * @uses SearchRepositoryAPI::getInstance Defining the instance
	 * @return mixed
	 */
	public static function getRepository( $type = null, $instance = null ){

		//Returning
		return ( is_null( $instance ) ) ? SearchRepositoryDBR::getInstance( $type ) : SearchRepositoryAPI::getInstance();

	}

	/**
	 * Searching by your identifier
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param integer $id Identifier
	 * @uses Validation::numericFields Validating the numeric fields
	 * @uses self::getRepository Getting the repository
	 * @uses SearchRepositoryDBR->byId Searching by your identifier
	 * @uses SearchController::byId Searching by your identifier
	 * @uses ListController::relatedByContentOrderly Listing the related content orderly
	 * @return stdclass
	 */
	public static function byId( $type = null, $id = 1 ){

		//Validating
		Validation::numericFields( [ 'identificador' => $id ] );

		//Searching and defining
		$object	= self::getRepository( $type )->byId( $id );

		//Checking
		if( $type == 'institutional' )
			//Defining
			$object->images	= ListController::relatedByContentOrderly( 'institutional-gallery', $object->id );
		else if( $type == 'contact' )
			//Defining
			$object->subject	= self::getRepository( 'subject' )->byId( $object->subject );

		//Returning
		return $object;

	}

	/**
	 * Searching by your slug
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param integer $id Identifier
	 * @uses Validation::emptyFields Validating the empty fields
	 * @uses self::getRepository Getting the repository
	 * @uses SearchRepositoryDBR->bySlug Searching by your slug
	 * @return stdclass
	 */
	public static function bySlug( $type = null, $slug = null ){

		//Validating
		Validation::emptyFields( [ 'slug' => $slug ] );

		//Returning
		return self::getRepository( $type )->bySlug( $slug );

	}

	/**
	 * Searching the movie
	 *
	 * @access public
	 * @param integer $id Movie's identifier
	 * @param string $url URL
	 * @uses self::getRepository Getting the repository
	 * @uses SearchRepositoryAPI->movie Searching the movie
	 */
	public static function movie( $id, $url ){

		self::getRepository( null, 'API' )->movie( $id, $url );

	}

}