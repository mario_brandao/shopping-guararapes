<?php
/**
 * Serach repository
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage search
 * @version 1.0
 * @since 2018/04/09 19:25
 */
Class SearchRepositoryAPI{

	/**
	 * Instance
	 *
	 * @access private
	 * @static
	 * @var SerachRepositoryAPI
	 */
	private static $instance = null;

	/**
	 * Defining the instance
	 *
	 * @access public
	 * @return SerachRepositoryAPI
	 */
	public static function getInstance(){

		//Checking
		if( is_null( self::$instance ) )
			//Defining
			self::$instance	= new self();

		//Returning
		return self::$instance;

	}

	/**
	 * Creating the movie's json
	 *
	 * @access public
	 * @param integer $id Movie's identifier
	 * @uses Util::formatStringUrl Formatting the string to URL format
	 */
	public function movie( $id, $url ){

		//Defining
		$movie	= json_decode( file_get_contents( INGRESSO_COM_API_URL . '/events/' . $id . '/partnership/' . INGRESSO_COM_API_PARTERSHIP ) );

		//Checking
		if( isset( $movie ) ){

			//Defining
			$trailer	= ( isset( $movie->trailers ) && count( $movie->trailers ) > 0 ) ? trim( $movie->trailers[ 0 ]->embeddedUrl ) : null;

			//Listing
			foreach( $movie->genres as $genre )
				//Defining
				$genres[]	= trim( $genre );

			//Defining the movie's sessions
			$data	= json_decode( file_get_contents( INGRESSO_COM_API_URL . '/sessions/city/' . INGRESSO_COM_API_CITY . '/event/url-key/' . trim( $movie->urlKey ) . '/partnership/' . INGRESSO_COM_API_PARTERSHIP ) );

			//Checking
			if( isset( $data ) ){

				//Listing
				foreach( $data as $theaters ){

					//Listing
					foreach( $theaters->theaters as $theater ){

						//Listing
						foreach( $theater->rooms as $room ){

							//Defining
							$sessions	= [];

							//Listing
							foreach( $room->sessions as $session ){

								//Defining
								$types	= [];

								//Listing
								foreach( $session->type as $type )
									//Defining
									$types[]	= $type;

								//Defining
								$sessions[]	= [ 'types' => $types, 'time' => $session->time ];

							}

							//Defining
							$rooms[ $theaters->date ][]	= [ 'name' => $room->name, 'sessions' => $sessions ];

						}

					}

				}

				//Crating
				file_put_contents( BASE_DIR_REPOSITORY . '/json/cinema/movie/' . Util::formatStringUrl( trim( $movie->title ) ) . '.json', json_encode( [ 'url' => $url, 'slug' => Util::formatStringUrl( trim( $movie->title ) ), 'title' => trim( $movie->title ), 'original_title' => trim( $movie->originalTitle ), 'rating' => trim( $movie->contentRating ), 'poster' => trim( $movie->images[ 0 ]->url ), 'featured' => trim( $movie->images[ 1 ]->url ), 'duration' => trim( $movie->duration ), 'synopsis' => trim( $movie->synopsis ), 'director' => trim( $movie->director ), 'cast' => trim( $movie->cast ), 'trailer' => $trailer, 'genres' => implode( ', ', $genres ), 'rooms' => $rooms ] ) );

			}

		}

	}

}