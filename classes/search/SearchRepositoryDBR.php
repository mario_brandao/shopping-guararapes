<?php
/**
 * Search repository
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage list
 * @version 1.0
 * @since 2018/04/09 19:25
 */
Class SearchRepositoryDBR{

	/**
	 * Connection's object
	 *
	 * @access private
	 * @var PDO
	 */
	private $connection;

	/**
	 * Showing or not the exception
	 *
	 * @access private
	 * @var boolean
	 */
	private $exception	= true;

	/**
	 * Instance
	 *
	 * @access private
	 * @static
	 * @var SearchRepositoryDBR
	 */
	private static $instance = [];

	/**
	 * Class constructor
	 *
	 * @access public
	 * @param string $type Type
	 * @uses ConnectionDBR::getInstance Defining the instance
	 */
	public function __construct( $type ){

		$this->connection	= ConnectionDBR::getInstance()->getConnection();
		$this->type			= $type;

	}

	/**
	 * Getting the instance
	 *
	 * @access public
	 * @param string $type Type
	 * @return ConnectionDBR
	 */
	public static function getInstance( $type ){

		//Checking
		if( !array_key_exists( $type, self::$instance ) )
			//Defining
			self::$instance[ $type ]	= new self( $type );

		//Returning
		return self::$instance[ $type ];

	}

	/**
	 * Searching
	 *
	 * @access public
	 * @param string $complement Complement
	 * @param array $params Params
	 * @throws Exception
	 * @return mixed
	 */
	public function search( $complement = null, $params = [] ){

		try{

			//Verificando
			switch( $this->type ){

				//News
				case 'news':

					//Definindo a consulta
					$sql	= 'SELECT * FROM news TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Sustainability
				case 'sustainability':

					//Definindo a consulta
					$sql	= 'SELECT * FROM sustainability TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Metadatas
				case 'metadata':

					//Definindo a consulta
					$sql	= 'SELECT * FROM metadata TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Store
				case 'store':

					//Definindo a consulta
					$sql	= 'SELECT TBL.*, TBL2.type FROM store TBL INNER JOIN store_category TBL2 ON (TBL.category = TBL2.id) WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Institutional's schedule
				case 'institutional-schedule':

					//Definindo a consulta
					$sql	= 'SELECT * FROM institutional_schedule TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Institutional's contact
				case 'institutional-contact':

					//Definindo a consulta
					$sql	= 'SELECT * FROM institutional_contact TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Institutional's cinema
				case 'institutional-cinema':

					//Definindo a consulta
					$sql	= 'SELECT * FROM institutional_cinema TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Institutional's sustainability
				case 'institutional-sustainability':

					//Definindo a consulta
					$sql	= 'SELECT * FROM institutional_sustainability TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Institutional's advertise
				case 'institutional-advertise':

					//Definindo a consulta
					$sql	= 'SELECT * FROM institutional_advertise TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Institutional
				case 'institutional':

					//Definindo a consulta
					$sql	= 'SELECT * FROM institutional TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Configuration
				case 'configuration':

					//Definindo a consulta
					$sql	= 'SELECT * FROM setting TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Contact
				case 'contact':

					//Definindo a consulta
					$sql	= 'SELECT * FROM contact TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

				//Subject
				case 'subject':

					//Definindo a consulta
					$sql	= 'SELECT * FROM subject TBL WHERE TBL.id IS NOT NULL ' . $complement;

				break;

			}

			//Preparing
			$resource	= $this->connection->prepare( $sql );

			//Checking
			if( count( $params ) > 0 )
				//Listing
				foreach( $params as $field => $configuration )
					//Defining
					$resource->bindValue( $field, $configuration[ 'value' ], $configuration[ 'type' ] );

			//Executing
			$resource->execute();

			//Defining
			$record	= $resource->fetch( PDO::FETCH_OBJ );

			//Verificandog
			if( $record )
				//Retornando
				return $record;
			else
				//Throwing the exception
				throw new Exception( 'Nenhum registro encontrado.' );

		}catch( PDOException $e ){

			//Throwing the exception
			throw new Exception( 'Ocorreu um erro durante a procura. Por favor, tente novamente mais tarde.' );

		}

	}

	/**
	 * Searching by your identifier
	 *
	 * @access public
	 * @param integer $id Identifier
	 * @param string $complement Complement
	 * @param string $params Params
	 * @uses $this->search Searching
	 * @return stdclass
	 */
	public function byId( $id, $complement = null, $params = [] ){

		//Defining
		$complement	= 'AND TBL.id = :id ' . $complement;

		//Defining
		$params		= array_merge( [ ':id' => [ 'value' => $id, 'type' => PDO::PARAM_INT ] ], $params );

		//Searching
		return $this->search( $complement, $params );

	}

	/**
	 * Searching by your slug
	 *
	 * @access public
	 * @param string $slug Slug
	 * @param string $complement Complement
	 * @param string $params Params
	 * @uses $this->search Searching
	 * @return stdclass
	 */
	public function bySlug( $slug, $complement = null, $params = [] ){

		//Defining
		$complement	= 'AND TBL.slug = :slug ' . $complement;

		//Defining
		$params		= array_merge( [ ':slug' => [ 'value' => $slug, 'type' => PDO::PARAM_STR ] ], $params );

		//Searching
		return $this->search( $complement, $params );

	}

}