<?php
/**
 * Util's class
 *
 * @abstract
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage util
 * @version 1.0
 * @since 1.0 2016/10/11 20:22
 */
Class Util{

	/**
	 * Defining the description
	 *
	 * @access public
	 * @static
	 * @param string $text Text
	 * @param integer $wordQuantity Word's quantity
	 * @param boolean $strip Remove or not the strip tags
	 * @return string
	 */
	public static function defineDescription( $text, $wordQuantity = 30, $strip = true ){

		//Defining
		$word		= explode( ' ', preg_replace( "/\r\n|\r|\n|\t/", '', strip_tags( $text ) ) );

		//Defining
		$quantity	= count( $word );

		//Checking
		if( $quantity <= $wordQuantity )
			//Returning
			return ( $strip ) ? rtrim( ltrim( trim( strip_tags( $text ) ) ) ) : $text;
		else{

			//Defining
			$text	= null;

			//Listing
			for( $intI = 0; $intI < $wordQuantity; $intI++ )
				//Defining
				$text .= $word[ $intI ] . ' ';

			//Defining
			$text	.= '...';

			//Returning
			return ( $strip ) ? rtrim( ltrim( trim( strip_tags( $text ) ) ) ) : $text;

		}

	}

	/**
	 * Defining the destination's options
	 *
	 * @access public
	 * @static
	 * @param integer $destination Destination
	 * @return stdclass
	 */
	public static function defineDestinationOptions( $destination ){

		//Defining
		$object	= new stdclass();

		//Checking
		switch( $destination ){

			//Contact
			case DESTINATION_CONTACT:

				//Defining
				$object->type			= 'contact';
				$object->answer_table	= 'contact_answer';
				$object->table			= 'contact';

			break;

		}

		//Returning
		return $object;

	}

	/**
	 * Defining the movie's type
	 *
	 * @access public
	 * @static
	 * @return stdclass
	 */
	public static function defineMovieType( $array ){

		//Defining
		$object	= new stdclass();

		//Listing
		foreach( $array as $type ){

			//Checking
			if( $type == 'Dublado' )
				//Defining
				$object->dub	= true;

			//Checking
			if( $type == 'Legendado' )
				//Defining
				$object->leg	= true;

			//Checking
			if( $type == '3D' )
				//Defining
				$object->has3d	= true;

			//Checking
			if( $type == 'Nacional' )
				//Defining
				$object->nac	= true;

		}

		//Returning
		return $object;

	}

	/**
	 * Defining the database order
	 *
	 * @access public
	 * @static
	 * @param string $order Order
	 * @return string
	 */
	public static function defineDatabaseOrder( $order = null ){

		//Checking
		switch( $order ){

			//Sequence
			case 'sequence':

				//Returning
				return 'sequence';

			break;

			//Random
			case 'random':

				//Returning
				return 'RAND()';

			break;

			//Date
			case 'date':

				//Returning
				return 'date';

			break;

			//Date DESC
			case 'date-desc':

				//Returning
				return 'date DESC';

			break;

			//Name
			case 'name':

				//Returning
				return 'name';

			break;

			//Title
			case 'title':

				//Returning
				return 'title';

			break;

			//ID
			case 'id':

				//Returning
				return 'id';

			break;

			//Default
			default:

				//Returning
				return 'id DESC';

			break;

		}

	}

	/**
	 * Removing specials characters
	 *
	 * @access public
	 * @static
	 * @param string $string String
	 * @return string
	 */
	public static function removeSpecialChar( $string ){

		//Defining
		$accents	= [ "Á","É","Í","Ó","Ú","Â","Ê","Î","Ô","Û","Ã","Ñ","Õ","Ä","Ë","Ï","Ö","Ü","À","È","Ì","Ò","Ù","á","é","í","ó","ú","â","ê","î","ô","û","ã","ñ","õ","ä","ë","ï","ö","ü","à","è","ì","ò","ù",".",",",":",";","...","ç","%","?","/","\\","”","“","'","!","@","#","$","&","*","(",")","+","=","{","}","[","]","|","<",">","\"","&ordf;","&ordm;","&deg;","‘","‘","&raquo;","","ª","º","»","´","Ç","’","&ndash;","&nbsp;","–","″","°","~","³", "`", "^", "---", "®", "©" ];
		$noAccents	= [ "a","e","i","o","u","a","e","i","o","u","a","n","o","a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","a","n","o","a","e","i","o","u","a","e","i","o","u","","","","","","c","_porcento","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","c","","","","","","","","","", "", "-", "","" ];

		//Returning
		return str_replace( $accents, $noAccents, $string );

	}

	/**
	 * Formatting string to URL format
	 *
	 * @access public
	 * @static
	 * @param string $string String
	 * @uses self::removeSpecialChar Removing special char
	 */
	public static function formatStringUrl( $string ){

		//Returning
		return strtolower( str_replace( ' ', '-', self::removeSpecialChar( html_entity_decode( $string, ENT_QUOTES, 'UTF-8' ) ) ) );

	}

	/**
	 * Formatting the phone
	 *
	 * @access public
	 * @static
	 * @param string $phone
	 * @return string
	 */
	public static function formatPhone( $phone ){

		//Returning
		return str_replace( [ '.', ' ', '-', '(', ')' ], null, $phone );

	}

	/**
	 * Formatting the date
	 *
	 * @access public
	 * @static
	 * @param string $date Date
	 * @param string $type Type
	 * @return mixed
	 */
	public static function formatDate( $date, $type = 'default' ){

		//Checking
		if( !is_null( $date ) ){

			//Defining
			$date	= new DateTime( str_replace( '/', '-', str_replace( '-', '', $date ) ) );

			//Checking
			switch( $type ){

				//Database without hour
				case 'database-no-hour':

					//Defining
					$date	= $date->format( 'Y-m-d' );

				break;

				//database
				case 'database':

					//Defining
					$date	= $date->format( 'Y-m-d H:i:s' );

				break;

				//Default without hour
				case 'default-no-hour';

					//Defining
					$date	= $date->format( 'd/m/Y' );

				break;

				//Hour and minute
				case 'hour-minute';

					//Defining
					$date	= $date->format( 'H:i' );

				break;

				//Messages
				case 'message';

					//Defining
					$date	= $date->format( 'd.m.Y - H:i' );

				break;

				//Messages
				case 'message';

					//Defining
					$date	= $date->format( 'd.m.Y - H:i' );

				break;

				//Cinema
				case 'cinema':

					//Defining
					$date	= $date->format( 'd/m' );

				break;

				//Site
				case 'site':

					//Defining
					$date	= $date->format( 'd/m/Y' );

				break;

				//Day of week
				case 'day-of-week':

					//Defining
					$date	= strftime( '%a', strtotime( $date->format( 'd-m-Y' ) ) );

				break;

				//Separated
				case 'separate':

					//Defining
					$dateHour	= explode( ' ', $date->format( 'Y-m-d H:i:s' ) );

					//Defining the array
					$dates		= explode( '-', $dateHour[ 0 ] );
					$hour		= explode( ':', $dateHour[ 1 ] );

					//Definindo
					$object		= new stdclass();

					//Defining the values
					$object->date			= $dateHour[ 0 ];
					$object->hours			= $dateHour[ 1 ];
					$object->day			= $dates[ 2 ];
					$object->month			= $dates[ 1 ];
					$object->month_long		= strftime( "%B", strtotime( $date->format( 'd-m-Y' ) ) );
					$object->month_short	= strftime( "%b", strtotime( $date->format( 'd-m-Y' ) ) );
					$object->year			= $dates[ 0 ];
					$object->hour			= $hour[ 0 ];
					$object->minute			= $hour[ 1 ];
					$object->second			= $hour[ 2 ];

					//Defining
					$date	= $object;

				break;

				//Default
				default:

					//Defining
					$date	= $date->format( 'd/m/Y - H:i' );

				break;

			}

			//Returning
			return ( !$date instanceof stdclass ) ? str_replace( [ ' - 00:00:00', ' - 00:00' ], '', $date ) : $date;

		}else
			//Returning
			return null;

	}

	/**
	 * Formatting the fields
	 *
	 * @access public
	 * @static
	 * @param array $fields Fields
	 * @return array
	 */
	public static function formatFields( $fields = [] ){

		//Listing
		foreach( $fields as $key => $field )
			//Defining
			$fields[ $key ]	= ( isset( $fields[ $key ] ) && $fields[ $key ] != '' ) ? $fields[ $key ] : null;

		//Returning
		return $fields;

	}

}