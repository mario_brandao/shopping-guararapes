<?php
/**
 * Validation class
 *
 * @author Lucas Dantas <lucas@caju.nu>
 * @package classes
 * @subpackage validation
 * @version 1.0
 * @since 2018/04/11 12:05
 */
Class Validation{

	/**
	 * Validating forms
	 *
	 * @access public
	 * @param string $form Form
	 * @param array $fields Fields
	 * @param mixed $file File
	 * @uses self::emptyFields Validating the empty fields
	 * @uses self::numericFields Validating the numeric fields
	 * @uses self::captcha Validating the captcha
	 */
	public static function form( $form = null, $fields = [], $file = null ){

		//Checking
		switch( $form ){

			//Contact
			case 'contact':

				//Validating
				self::emptyFields( [ 'nome' => $fields[ 'nome' ], 'telefone' => $fields[ 'telefone' ], 'mensagem' => $fields[ 'mensagem' ] ] );
				self::numericFields( [ 'assunto' => $fields[ 'assunto' ] ] );
				self::email( $fields[ 'email' ] );
				self::captcha( $fields[ 'g-recaptcha-response' ] );

			break;

		}

	}

	/**
	 * Validating the file's type
	 *
	 * @access public
	 * @static
	 * @param string $type Type
	 * @param array $file File
	 * @throws Exception
	 */
	public static function fileType( $type, $file ){

		//Checking the type
		switch( $type ){

			//PDF and DOC
			case 'pdf-doc':

				//Checking
				if( strpos( $file[ 'type' ], 'application/pdf' ) === false && strpos( $file[ 'type' ], 'application/msword' ) === false && strpos( $file[ 'type' ], 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) === false )
					//Throwing the exception
					throw new Exception( 'O arquivo passado não é válido. Apenas PDF ou DOC.' );

			break;

		}

	}

	/**
	 * Validating empty fields
	 *
	 * @access public
	 * @static
	 * @param array $fields Fields
	 * @throws Exception
	 */
	public static function emptyFields( $fields = [] ){

		//Listing
		foreach( $fields as $description => $content )
			//Checking
			if( is_null( $content ) )
				//Throwing the exception
				throw new Exception( 'O campo "' . $description . '" está vazio.' );

	}

	/**
	 * Validating numeric fields
	 *
	 * @access public
	 * @static
	 * @param array $fields Fields
	 * @throws Exception
	 */
	public static function numericFields( $fields = [] ){

		//Listing
		foreach( $fields as $description => $value )
			//Checking
			if( is_null( $value ) || !is_numeric( $value ) )
				//Throwing the exception
				throw new Exception( 'O campo "' . $description . '" não é numérico.' );

	}

	/**
	 * Validating the e-mail
	 *
	 * @access public
	 * @static
	 * @param string $email E-mail
	 * @throws Exception
	 */
	public static function email( $email = null ){

		//Checking
		if( !filter_var( $email, FILTER_VALIDATE_EMAIL ) )
			//Throwing the exception
			throw new Exception( 'O e-mail "' . $email . '" não é válido.' );

	}

	/**
	 * Validating the date
	 *
	 * @access public
	 * @static
	 * @param array $array Dates
	 * @throws Exception
	 */
	public static function date( $array ){

		try{

			//Listing
			foreach( $array as $date )
				//Defining
				new DateTime( str_replace( '/', '-', $date ) );

		}catch( Exception $e ){

			//Throwing the exception
			throw new Exception( 'A data "' . $date . '" está inválida.' );

		}

	}

	/**
	 * Validating the array
	 *
	 * @access public
	 * @static
	 * @param array $array Array
	 * @throws Exception
	 */
	public static function array( $array = [] ){

		//Listing
		foreach( $array as $description => $value )
			//Checking
			if( count( $value ) == 0 )
				//Throwing the exception
				throw new Exception( 'O campo "' . $description . '" está vazio.' );
			else{

				//Defining
				$filled	= false;

				//Listing
				foreach( $value as $content )
					//Checking
					if( !empty( $content ) )
						//Defining
						$filled	= true;

				//Checking
				if( !$filled )
					//Throwing the exception
					throw new Exception( 'O campo "' . $description . '" está vazio.' );

			}

	}

	/**
     * Validating the captcha
     *
     * @access public
     * @static
     * @param string $value Value
     * @throws Exception
     */
	public static function captcha( $value ){

		//Defining
		$objeto	= json_decode( file_get_contents( CAPTCHA_URL . '?secret=' . CAPTCHA_KEY . '&response=' . $value ) );

        //Checking
        if( !$objeto->success )
        	//Throwing the excpetion
            throw new Exception( 'O captcha está inválido. Por favor, tente novamente.' );

    }

}