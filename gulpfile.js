'use strict';

var jshint = require('gulp-jshint');
var gulp = require('gulp');

// GENERAL
var runSequence = require('run-sequence');
var bs = require('browser-sync').create();

// CSS
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var spritesmith = require('gulp.spritesmith');
var autoprefixer = require('gulp-autoprefixer');

// JS
var jshint_stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');
var pump = require('pump');

// IMAGES
var imagemin = require('gulp-imagemin');

var assets = {
  node_modules: 'node_modules',
  scss: 'assets/scss',
  imgs: 'assets/imgs',
  icons: 'assets/images/icons',

  // generating for dev
  css: 'assets/css',
  js: 'assets/js',
  images: 'assets/img',
  scripts: 'assets/scripts',

};

// CSS
gulp.task('styles', function () {

  // DEVELOPMENT
  gulp.src([
    assets.scss + '/style.scss'
  ])
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(concat('style.css'))
    .pipe(cssmin())
    .pipe(gulp.dest(assets.css));
});

// JS
gulp.task('scripts', function () {
  var files = [
    assets.scripts + '/jquery.min.js',
    assets.scripts + '/libs/*.js',
    assets.scripts + '/app/script.js'
  ];

  pump([
    gulp.src(files),
    concat('build.js'),
    // uglify(),
    gulp.dest(assets.js)
  ]);

});

// IMAGES
// gulp.task('images', function () {
//   var path = assets.imgs + '/**/*';

//   // DEVELOPMENT
//   gulp.src(path)
//     .pipe(imagemin())
//     .pipe(gulp.dest(assets.images));
// });

gulp.task('reload', function () {
  bs.reload();
});

gulp.task('serve', function () {

  bs.init({
    proxy: 'localhost'
  });

  gulp.watch(assets.scss + '/**/*.scss').on('change', function () {
    setTimeout(() => {
      runSequence('styles', 'reload');
    }, 120);
  });

  gulp.watch(assets.scripts + '/**/*.js').on('change', function () {
    runSequence('scripts', 'reload');
  });

  gulp.watch(assets.imgs + '/**/*').on('change', function () {
    runSequence('images', 'reload');
  });
});

gulp.task('default', ['styles', 'scripts', 'images', 'serve']);