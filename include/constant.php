
<?php
/*
 * Main constants
 */

//Checking
if( $_SERVER[ 'SERVER_NAME' ] == 'localhost' ){

	//Defining the base dir root
	define( 'BASE_DIR_ROOT', 'D:/wamp/www/shopping-guararapes' );
	//Defining the base dir
	define( 'BASE_DIR', BASE_DIR_ROOT );
	//Defining the base url
	define( 'BASE_URL', 'http://localhost/shopping-guararapes' );
	//Defining the base url for repository
	define( 'BASE_URL_REPOSITORY', 'http://repositorio.caju.ag/shopping-guararapes/' );

	//Defining the database address
	define( 'DB_ADDRESS', 'localhost' );
	//Defining the database base
	define( 'DB_BASE', 'guararapes' );
	//Defining the database port
	define( 'DB_PORT', 3306 );
	//Defining the database user
	define( 'DB_USER', 'root' );
	//Defining the database password
	define( 'DB_PASSWORD', '' );

	//Defining the debug
	define( 'DEBUG', true );

	//Defining if is to minify the html
	define( 'HTML_MINIFY', false );

	//Defining the metadata's path
	define( 'METADATA', '/Library/Webserver/Documents/controle/files/shopping-guararapes/metadata/metadata.php' );

	//Checking
	if( file_exists( METADATA ) )
		//Including
		require( METADATA );

}else{

	//Defining the base dir root
	define( 'BASE_DIR_ROOT', '/home/guararapes/public_html' );
	//Defining the base dir
	define( 'BASE_DIR', BASE_DIR_ROOT . '/current' );
	//Defining the base url
	define( 'BASE_URL', 'http://guararapes.caju.ag' );
	//Defining the repository's base url
	define( 'BASE_URL_REPOSITORY', BASE_URL . '/files' );

	//Defining the debug
	define( 'DEBUG', false );

	//Defining if is to minify the html
	define( 'HTML_MINIFY', false );

	//including the project's constant
	require( BASE_DIR_ROOT . '/setting/constant.php' );

}

//Defining the assets's base dir
define( 'BASE_DIR_ASSETS', BASE_DIR . '/assets' );
//Defining the template's base dir
define( 'BASE_DIR_TEMPLATE', BASE_DIR_ASSETS . '/html' );
//Defining the repository's base dir
define( 'BASE_DIR_REPOSITORY', BASE_DIR_ROOT . '/files' );

//Defining the image's base url
define( 'BASE_URL_IMG', BASE_URL . '/assets/img' );
//Definng the script's base url
define( 'BASE_URL_JS', BASE_URL . '/assets/js' );
//Defining the style's base url
define( 'BASE_URL_CSS', BASE_URL . '/assets/css' );
//Defining the base url for cinema
define( 'BASE_URL_CINEMA', BASE_URL_REPOSITORY . '/json/cinema' );
//Defining the base url for image's share
define( 'BASE_URL_IMG_SHARE', BASE_URL_IMG . '/base/compartilhamento.jpg' );
//Defining the base url for news's no image
define( 'BASE_URL_IMG_IMAGE_NO_NEWS_SMALL', BASE_URL_IMG . '/base/compartilhamento.jpg' );
//Defining the base url for news's no image
define( 'BASE_URL_IMG_IMAGE_NO_NEWS_MEDIUM', BASE_URL_IMG . '/base/compartilhamento.jpg' );
//Defining the base url for news's no image
define( 'BASE_URL_IMG_IMAGE_NO_NEWS_BIG', BASE_URL_IMG . '/base/compartilhamento.jpg' );

//Defining the store's category as food
define( 'STORE_CATEGORY', [ 1 => 'Alimentação', 2 => 'Lojas', 3 => 'Serviços' ] );

//Defining the cinema's api URL
define( 'INGRESSO_COM_API_URL', 'https://api-content.ingresso.com/v0' );
//Defining the cinema's api partnership
define( 'INGRESSO_COM_API_PARTERSHIP', 'shpguararapes' );
//Defining the cinema's api city
define( 'INGRESSO_COM_API_CITY', 54 );
//Defining the cinema's api theater
define( 'INGRESSO_COM_API_THEATER', 1070 );

//Defining the captcha's URL
define( 'CAPTCHA_URL', 'https://www.google.com/recaptcha/api/siteverify' );
//Defining the captcha's key
define( 'CAPTCHA_KEY', '6Lf4nGMUAAAAAD_gqE_gsCizzq7q4zZsVEPzZYsh' );
//Defining the captcha's site key
define( 'CAPTCHA_KEY_SITE', '6Lf4nGMUAAAAAPSQ88C3oDRqpmKt2OeWl103I6Qq' );

//Defining the origin as internal
define( 'SOURCE_SITE_INTERNAL', 1 );

//Defining the records per page
define( 'RECORDS_PER_PAGE', 15 );

//Defining the contact's destination
define( 'DESTINATION_CONTACT', 1 );

//Defining the twitter's username
define( 'TWITTER_USERNAME', '@sguararapes' );

//Defining the HASH
define( 'HASH', 'Gu4rAr4p3!s!!f3!o7' );
//Defining the project's name
define( 'PROJECT_NAME', 'Shopping Guararapes' );
//Defining the system's name
define( 'SYSTEM_NAME', 'Controle' );
//Defining the system's url
define( 'SYSTEM_URL', 'https://controle.io' );

//Defining the e-mails's server
define( 'EMAIL_SERVER', 'agenciacaju.caju.ag' );
//Defining the e-mails's port
define( 'EMAIL_PORT', 465 );
//Defining the e-mails's security's type
define( 'EMAIL_SECURITY', 'ssl' );
//Defining the e-mails's user
define( 'EMAIL', 'noreply@controle.io' );
//Defining the e-mails's password
define( 'EMAIL_PASSWORD', '3c9mfu026200' );
//Defining the e-mails's return's path
define( 'EMAIL_RETURN_PATH', null );