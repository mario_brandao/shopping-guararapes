<?php
/*
* Template's variables
*/

//Adding the head
$template->addFile( 'INCLUDE_HEADER', BASE_DIR_TEMPLATE . '/include/header.html' );

//Adding the top
$template->addFile( 'INCLUDE_HEAD', BASE_DIR_TEMPLATE . '/include/head.html' );

//Adding the footer
$template->addFile( 'INCLUDE_FOOTER', BASE_DIR_TEMPLATE . '/include/footer.html' );

//Adding the scripts
$template->addFile( 'INCLUDE_JAVASCRIPT', BASE_DIR_TEMPLATE . '/include/javascript.html' );

//Metadatas
try{

	//Checking
	if( isset( $meta ) ){

		//Defining
		$object							= SearchController::byId( 'metadata', $meta );

		//Defining
		$template->METADATA_TITLE		= $object->title;
		$template->METADATA_DESCRIPTION	= $object->description;
		$template->METADATA_KEYWORDS	= $object->keyword;

	}

}catch( Exception $e ){}

//Configuration
try{

	//Defining
	$object	= SearchController::byId( 'configuration' );

	//Checking
	if( $object->maintenance )
		//Redirecting
		header( 'Location:' . BASE_URL . '/manutencao' );

	//Defining the codes to template
	$template->CODE_TOP		= $object->top;
	$template->CODE_MIDDLE	= $object->middle;
	$template->CODE_FOOTER	= $object->footer;

	//Checking
	if( $page->module == 'sucesso' )
		//Defining the code to template
		$template->CODE_SUCCESS	= $object->success;

}catch( Exception $e ){}

//Store's categories
try{

	//Listing
	foreach( STORE_CATEGORY as $id => $type ){

		//Defining the type's name to template
		$template->TYPE_NAME	= $type;

		//Defining
		$records				= ListController::relatedByContentOrderly( 'category-by-type', $id, 'name' );

		//Checking
		if( !is_null( $records ) )
			//Listing
			foreach( $records as $record ){

				//Defining the object to template
				$template->OBJ_CATEGORY	= $record;

				//Checking
				if( $page->module == 'lojas' )
					//Showing the block
					$template->block( 'CATEGORIES_STORE' );

				//Showing the block
				$template->block( 'CATEGORIES' );

			}

		//Showing the block
		$template->block( 'TYPES' );

		//Checking
		if( $page->module == 'lojas' )
			//Showing the block
			$template->block( 'TYPES_STORE' );

	}

}catch( Exception $e ){}

//Institutional's schedule
try{

	//Defining the object to template
	$template->OBJ_INSTITUTIONAL_SCHEDULE	= SearchController::byId( 'institutional-schedule' );

}catch( Exception $e ){}

//Defining some variables
$template->BASE_URL				= BASE_URL;
$template->BASE_URL_CSS			= BASE_URL_CSS;
$template->BASE_URL_IMG			= BASE_URL_IMG;
$template->BASE_URL_JS			= BASE_URL_JS;
$template->PROJECT_NAME			= PROJECT_NAME;
$template->BASE_URL_REPOSITORY	= BASE_URL_REPOSITORY;
$template->TWITTER_USERNAME		= TWITTER_USERNAME;